package com.xwady.core;

import android.content.res.Configuration;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.UserUtils;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.Locale;


public class CustomApplication extends MultiDexApplication {

    SessionManager sessionManager;
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private User user;


    @Override
    public void onCreate() {
        sessionManager=new SessionManager(this);
        user=sessionManager.getUserDetails();
        if (null==user) {
            user = new User();
            user.lang="en";
            sessionManager.createLoginSession(user);
        }

            super.onCreate();
        loadDData();
        Fonty
                .context(this)
                .regularTypeface("DIN Next LT W23 Regular.ttf")
                .boldTypeface("DIN Next LT W23 Bold.ttf")
                .done();

        super.onCreate();
        Locale loc = new Locale(/*user.lang*/"en");
        Locale.setDefault(loc);
        Configuration config = new Configuration();
        config.locale = loc;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        /*GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        D.mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        D.mGoogleApiClient.connect();*/

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        loadDData();
        Fonty
                .context(this)
                .regularTypeface("dinnextltarabic-light.ttf")
                .boldTypeface("dinnextltarabic-light.ttf")
                .done();

        super.onCreate();
        Locale loc = new Locale(/*user.lang*/"en");
        Locale.setDefault(loc);
        Configuration config = new Configuration();
        config.locale = loc;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

    }



    /**
     * Load the user object from disk.
     */
    private void loadDData() {
        D.sSelf = Utils.readObject(this, UserUtils.USER_FILE);
        D.fcm_token = Utils.readObject(this, UserUtils.FCM_TOKEN);
        if (null == D.sSelf) {
            D.sSelf = new User();
        }

        D.AccountStatus status = Utils.readObject(this, UserUtils.ACCOUNT_STATUS);
        if (null != status) {
            D.sAccountStatus = status;
        }

    }

}
