package com.xwady.core.helpers;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.xwady.core.R;

import java.util.List;

public class SpinnerAdapter extends ArrayAdapter<String> {
    // Initialise custom font, for example:
    Typeface font = Typeface.createFromAsset(getContext().getAssets(),
            "fonts/dinnextltarabic-light.ttf");

    // (In reality I used a manager which caches the Typeface objects)
    // Typeface font = FontManager.getInstance().getFont(getContext(), BLAMBOT);

    public SpinnerAdapter(Context context, int resource, List<String> items) {
        super(context, resource, items);
    }



    // Affects default (closed) state of the spinner
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTypeface(font);
        if (position == 0) {
            // Set the hint text color gray
            view.setTextColor(Color.parseColor("#5b5a58"));
        } else {
            view.setTextColor(getContext().getResources().getColor(R.color.black));
        }
        return view;
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTypeface(font);
        if (position == 0) {
            // Set the hint text color gray
            view.setTextColor(getContext().getResources().getColor(R.color.grey));
        } else {
            view.setTextColor(getContext().getResources().getColor(R.color.black));
        }
        return view;
    }

    @Override
    public boolean isEnabled(int position) {
        return position != 0;
    }
}