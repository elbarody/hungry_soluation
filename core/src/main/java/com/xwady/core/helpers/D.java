package com.xwady.core.helpers;


import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

//import com.google.android.gms.common.api.GoogleApiClient;
import com.xwady.core.models.User;
//import com.xwady.core.webService.model.worker.response.WorkerProfileResponse;

public class D {
    public static String fcm_token;
    public static User sSelf;
    //public static GoogleApiClient mGoogleApiClient;
    public static final String BASE_URL ="http://hungrysolution.com/api/";
    public static final String IMAGE_URL ="http://hungrysolution.com";
    public static final String USER_URL = "http://hungrysolution.com/api/customer/";
    public static final String DRIVER_URL = "http://hungrysolution.com/api/driver/";
    public static final String DISHES_URL = "http://hungrysolution.com/api/dishes/";
    public static final String ORDER_URL = "http://hungrysolution.com/api/order/";
    public static final String RESTAURANT_URL = "http://hungrysolution.com/api/restaurant/";



    //public static WorkerProfileResponse.ProfileData USER_PROFILE;

    public static User sOther;

    public static final String SELF = "com.xwady.sahlacore.SELF";
    public static final String OTHER = "com.xwady.sahlacore.OTHER";
    public static final String ACCOUNT_STATUS = "com.xwady.sahlacore.ACCOUNT_STATUS";

    public static AccountStatus sAccountStatus;

    private D(){
    }

    public enum AccountStatus {
        LOGGED_OUT,
        NOT_REGISTERED,
        NOT_ACTIVATED,
        NOT_ACTIVATED_SOCIAL,
        LOGGED_IN,
    }
    //check internet connection
    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(mContext.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
