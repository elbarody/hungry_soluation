package com.xwady.core.helpers;

import android.content.Context;

import com.xwady.core.models.User;

public class UserUtils {

    public static final String USER_FILE = "user";
    public static final String ACCOUNT_STATUS = "accountStatus";
    public static final String FCM_TOKEN = "fcm_token";

    private static final transient String TAG = User.class.getSimpleName();

    public static void saveUser(Context context) {
        Utils.writeObject(context, D.sSelf, USER_FILE);
        Utils.writeObject(context, D.sAccountStatus, ACCOUNT_STATUS);

    }
    public static void saveToken(Context context){
        Utils.writeObject(context,D.fcm_token,FCM_TOKEN);

    }

    public static void clearUser(Context context) {
        D.sSelf = new User();
        D.sAccountStatus = D.AccountStatus.LOGGED_OUT;
        saveUser(context);
    }
}
