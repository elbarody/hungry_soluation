package com.xwady.core.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import com.marcinorlowski.fonty.Fonty;

public class MyPagerAdapter extends FragmentPagerAdapter {
    private Fragment[] mFragments;

    public MyPagerAdapter(FragmentManager fm, Fragment... fragments) {
        super(fm);
        this.mFragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new Fragment();
        if (mFragments.length >= position) {
            fragment = mFragments[position];
//            Log.d("getItem: ",fragment.getTag());
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return mFragments.length;
    }
}
