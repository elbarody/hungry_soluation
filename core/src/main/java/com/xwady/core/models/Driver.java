package com.xwady.core.models;

import java.util.ArrayList;
import java.util.List;

public class Driver {
    public String id;
    public String success, lang,vehicleLicense,
            vehicleType,vehicleModel,driverLicense, token, mobile,
            email, address, district, activationCode, userName, password, city, date_of_birth;

    public String company_name;
    private AccountStatus loginStatus;
    public List<WorkDays> workDays = new ArrayList<>();
    public List<WorkArea> workAreas = new ArrayList<>();
    public List<CategoryRequest> categoryRequests = new ArrayList<>();
    public String id_national;
    public String national;
    public String type;
    public double lon, lat;
    public int in_trip;

    public boolean isInChatActivity = false;
    public String time_from, time_to;
    public String request_id;

    public AccountStatus getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(AccountStatus loginStatus) {
        this.loginStatus = loginStatus;
    }

    public enum AccountStatus {LOGGED_IN_CLIENT, LOGGED_IN_PROVIDER, LOGGED_OUT, NOT_ACTIVE, NOT_COMPLETE, LANG_CHOOSED, LOGGED_IN_DRIVER, L}
}

