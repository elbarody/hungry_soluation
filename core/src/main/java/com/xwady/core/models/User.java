package com.xwady.core.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable {
    public String id;
    public String success, lang, userName, token, status, category, career, specialization, verified,formal_img1,formal_img2,formal_img3, phone, social_name, email, city, area, gender, date_of_birth;
    public String area_id,area_name;
    public String cardNumber;
    public String socialId;
    public String fcm_token;
    public String img;
    public String platform;
    public String address;
    public String company_name;
    private AccountStatus loginStatus;
    public List<WorkDays> workDays=new ArrayList<>();
    public List<WorkArea> workAreas=new ArrayList<>();
    public List<CategoryRequest> categoryRequests=new ArrayList<>();
    public String password;
    public String id_national;
    public String national;
    public String type;
    public double lon,lat;
    public int in_trip;

    public boolean isInChatActivity=false;
    public String time_from,time_to;
    public String request_id;
    public String license;
    public String delivery_fee;

    public AccountStatus getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(AccountStatus loginStatus) {
        this.loginStatus = loginStatus;
    }

    public enum AccountStatus {LOGGED_IN_CLIENT, LOGGED_IN_PROVIDER, LOGGED_OUT, NOT_ACTIVE, NOT_COMPLETE, LANG_CHOOSED, LOGGED_IN_USER, LOGGED_IN_RESTAURANT, L}
}

