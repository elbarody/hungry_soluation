package com.hungry.oem.hungrysoluation.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.Model.RestaurantData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.view.Activity.ResturantDishesActivity;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.models.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class ResturantsRecyclerViewAdapter extends RecyclerView.Adapter<ResturantsRecyclerViewAdapter.BH> {

    private static final int FOOTER_VIEW = 1;
    User mUser;

    private List<RestaurantData> data;
    private Context mContext;
    private Dialog dialog;
    private String from;
    public static RestaurantData restaurantData ;

    public ResturantsRecyclerViewAdapter(List<RestaurantData> latestOffers, Context context, String from) {
        this.data = latestOffers;
        mContext = context;
        this.from = from;
    }

    @Override
    public BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.resturant_rec_row, parent, false);
        BH bh = new BH(v);
        Fonty.setFonts((ViewGroup) v);
        return bh;

    }

    @Override
    public void onBindViewHolder(BH holder, int position) {
        holder.itemView.setOnClickListener(v -> {
            // Toast.makeText(mContext,""+position,Toast.LENGTH_LONG).show();
            restaurantData = data.get(position);
            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            Intent intent = new Intent(mContext, ResturantDishesActivity.class);
            intent.putExtras(bundle);
            mContext.startActivity(intent);
        });
        holder.restName.setText(data.get(position).name);
        holder.locationTv.setText(data.get(position).address);
        holder.textAverage.setText("(" + data.get(position).votes + "vote,average" + data.get(position).average + ")");
        holder.ratingBar.setRating(data.get(position).rate);
        Glide.with(mContext).load(D.IMAGE_URL + data.get(position).logoImgeURL).error(R.drawable.macdonalds).into(holder.logo);


    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class BH extends RecyclerView.ViewHolder {
        @BindView(R.id.logo)
        ImageView logo;
        @BindView(R.id.rest_name)
        TextView restName;
        @BindView(R.id.rating_bar)
        MaterialRatingBar ratingBar;
        @BindView(R.id.text_average)
        TextView textAverage;
        @BindView(R.id.location_tv)
        TextView locationTv;
        @BindView(R.id.textView4)
        TextView textView4;

        BH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
