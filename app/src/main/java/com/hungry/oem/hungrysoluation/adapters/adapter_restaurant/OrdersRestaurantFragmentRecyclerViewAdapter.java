package com.hungry.oem.hungrysoluation.adapters.adapter_restaurant;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.hungry.oem.hungrysoluation.Model.RestaurantOrderData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.view.Activity.AddReviewActivity;
import com.hungry.oem.hungrysoluation.view.Activity.DriverLocationActivity;
import com.hungry.oem.hungrysoluation.view.Activity.ShowReviewsActivity;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.models.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by eng on 12/02/18.
 */

public class OrdersRestaurantFragmentRecyclerViewAdapter extends RecyclerView.Adapter<OrdersRestaurantFragmentRecyclerViewAdapter.BH> {

    private static final int FOOTER_VIEW = 1;
    User mUser;


    private List<RestaurantOrderData> data;
    private Context mContext;
    private Dialog dialog;
    private String from;

    public OrdersRestaurantFragmentRecyclerViewAdapter(List<RestaurantOrderData> latestOffers, Context context, String from) {
        this.data = latestOffers;
        mContext = context;
        this.from = from;
    }

    @Override
    public BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.rv_item_orders_restaurant_fragment, parent, false);
        BH bh = new BH(v);
        Fonty.setFonts((ViewGroup) v);
        return bh;

    }

    @Override
    public void onBindViewHolder(BH holder, int position) {
        if (from.equals("user")) {
            holder.textDate.setText(data.get(position).datetime.split(" ")[0]);
            holder.textTime.setText(data.get(position).datetime.split(" ")[1]);
            holder.participation_number_tv.setText(mContext.getString(R.string.participation_number) + data.get(position).id);
            holder.nameTv.setText(data.get(position).restaurantName);
            holder.mobileTV.setText(data.get(position).mobile + mContext.getString(R.string.phone_number));
            holder.restaurantName.setText(data.get(position).restaurantId);

            holder.constrain.setOnClickListener(v -> {
                Intent intent = new Intent();
                if (data.get(position).status.equals("1")) {
                    intiateDialogCancel();
                }else if (data.get(position).status.equals("-1")) {
                    intiateDialogCanceledOrder();
                }
                intent.putExtra("resturantId", data.get(position).restaurantId);
                intent.putExtra("orderId", data.get(position).id);
                mContext.startActivity(new Intent(mContext, DriverLocationActivity.class));
            });
        } else {
            holder.textDate.setText(data.get(position).datetime.split(" ")[0]);
            holder.textTime.setText(data.get(position).datetime.split(" ")[1]);
            holder.participation_number_tv.setText(mContext.getString(R.string.participation_number) + data.get(position).customerId);
            holder.nameTv.setText(data.get(position).customerName);
            holder.mobileTV.setText(data.get(position).mobile + mContext.getString(R.string.phone_number));
            holder.restaurantName.setText(data.get(position).customerName);
            holder.constrain.setOnClickListener(v -> {
                Intent intent = new Intent();
                intent.putExtra("resturantId", data.get(position).restaurantId);
                intent.putExtra("orderId", data.get(position).id);
                mContext.startActivity(new Intent(mContext, DriverLocationActivity.class));
            });
        }
    }

    private void intiateDialogCanceledOrder() {
        Dialog dialog = new Dialog(mContext, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_cancel);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        new Handler().postDelayed(() -> {
            dialog.dismiss();

        }, 2000);
    }

    private void intiateDialogCancel() {
        Dialog dialog = new Dialog(mContext, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_cancel);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        Button cancelBtn = dialog.findViewById(R.id.dialog_btn);
        cancelBtn.setOnClickListener(v -> {

        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class BH extends RecyclerView.ViewHolder {
        @BindView(R.id.text_date)
        TextView textDate;
        @BindView(R.id.time__tv)
        TextView textTime;
        @BindView(R.id.participation_number_tv)
        TextView participation_number_tv;
        @BindView(R.id.name_tv)
        TextView nameTv;
        @BindView(R.id.mobile_number_tv)
        TextView mobileTV;
        @BindView(R.id.constrain)
        ConstraintLayout constrain;
        @BindView(R.id.restaurant_name_tv)
        TextView restaurantName;

        BH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
