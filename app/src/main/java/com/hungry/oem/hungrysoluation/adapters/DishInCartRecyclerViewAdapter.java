package com.hungry.oem.hungrysoluation.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.view.Activity.DishDetailesActivity;
import com.hungry.oem.hungrysoluation.view.Activity.ItemInCartActivity;
import com.hungry.oem.hungrysoluation.view.Activity.MainActivity;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.models.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DishInCartRecyclerViewAdapter extends RecyclerView.Adapter<DishInCartRecyclerViewAdapter.BH> {

    private static final int FOOTER_VIEW = 1;
    User mUser;
    private List<DishesData> list_dishData;
    private Context mContext;
    private Dialog dialog;
    private String from;

    public DishInCartRecyclerViewAdapter(List<DishesData> list_dishData, Context context, String from) {
        this.list_dishData = list_dishData;
        mContext = context;
        this.from = from;
    }

    @Override
    public BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.dish_cart_row, parent, false);
        BH bh = new BH(v);
        Fonty.setFonts((ViewGroup) v);
        return bh;

    }

    @Override
    public void onBindViewHolder(BH holder, int position) {

        holder.Dish_name.setText(list_dishData.get(position).name);
        holder.Dish_count.setText("" + list_dishData.get(position).count);
        holder.Dish_Prce.setText(list_dishData.get(position).price);
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = list_dishData.get(position).count;
                int prices = Integer.parseInt(list_dishData.get(position).price);
                int pric = prices/count;

                count++;
                holder.Dish_count.setText("" + count);
                list_dishData.get(position).count = count;
                int total = pric * count;
                 list_dishData.get(position).price = String.valueOf(total);
                pric(mContext);
                holder.Dish_Prce.setText(list_dishData.get(position).price);
            }
        });

        holder.Minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int count = list_dishData.get(position).count;
                int prices = Integer.parseInt(list_dishData.get(position).price);
                int pric = prices/count;

                count--;
                holder.Dish_count.setText("" + count);
                list_dishData.get(position).count = count;
                int total = pric*count ;
                list_dishData.get(position).price = String.valueOf(total);
                pric(mContext);
                holder.Dish_Prce.setText(list_dishData.get(position).price);
            }
        });


        Glide.with(mContext).load(D.IMAGE_URL + list_dishData.get(position).imageUrl).error(R.drawable.background2).into(holder.dish_image);
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.card_list.remove(position);
                Remove(mContext, ItemInCartActivity.rec_item_cart, ItemInCartActivity.dishInCartRecyclerViewAdapter);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return list_dishData.size();
    }


    public class BH extends RecyclerView.ViewHolder {
        @BindView(R.id.dish_name)
        TextView Dish_name;
        @BindView(R.id.dish_price)
        TextView Dish_Prce;
        @BindView(R.id.num)
        TextView Dish_count;
        @BindView(R.id.remove_item)
        TextView remove;
        @BindView(R.id.dish_image)
        ImageView dish_image;
        @BindView(R.id.plus)
        ImageView plus ;
        @BindView(R.id.minus)
        ImageView Minus;

        BH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public static void Remove(Context mContext, RecyclerView recyclerView, DishInCartRecyclerViewAdapter adapter) {

        adapter = new DishInCartRecyclerViewAdapter(MainActivity.card_list, mContext, "");
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        pric(mContext);

    }


    public static void pric (Context mContext){
        int totaly = 0;
        for (int i = 0; i < MainActivity.card_list.size(); i++) {

            int total = Integer.parseInt(MainActivity.card_list.get(i).price);
            totaly += total;
        }
        ItemInCartActivity.sub_toal.setText((mContext.getString(R.string.subtotal) + "   " +totaly +"   "+ mContext.getString(R.string.sar)));
       int i = totaly + 10 ;
        ItemInCartActivity.total.setText((mContext.getString(R.string.total) +"   "+ i + "   " + mContext.getString(R.string.sar)));
    }
}
