package com.hungry.oem.hungrysoluation.webService.model.request;

/**
 * Created by oem on 9/19/18.
 */

public class UpdateCustmerRequest {
    private String id, userName, email, password, mobile, oldPassword;

    public UpdateCustmerRequest(String id, String userName, String email, String password, String mobile, String oldPassword) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.mobile = mobile;
        this.oldPassword = oldPassword;
    }
}
