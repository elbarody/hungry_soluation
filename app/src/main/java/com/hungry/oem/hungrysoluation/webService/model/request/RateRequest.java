package com.hungry.oem.hungrysoluation.webService.model.request;

/**
 * Created by oem on 10/30/18.
 */

public class RateRequest {

    private String customerId,restaurantId,value,comment;

    public RateRequest(String customerId, String restaurantId, String value, String comment) {
        this.customerId = customerId;
        this.restaurantId = restaurantId;
        this.value = value;
        this.comment = comment;
    }
}
