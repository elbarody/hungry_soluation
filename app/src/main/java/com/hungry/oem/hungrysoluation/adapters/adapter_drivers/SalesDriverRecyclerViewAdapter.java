package com.hungry.oem.hungrysoluation.adapters.adapter_drivers;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.Model.SalesData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.DishRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.adapters.MostPopularRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.view.Activity.DishDetailesActivity;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.models.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SalesDriverRecyclerViewAdapter extends RecyclerView.Adapter<SalesDriverRecyclerViewAdapter.BH> {

    private static final int FOOTER_VIEW = 1;
    User mUser;

    private List<SalesData> data;
    private Context mContext;
    private Dialog dialog;
    private String from;
    public static SalesData salesData;

    public SalesDriverRecyclerViewAdapter(List<SalesData> salesData, Context context, String from) {
        this.data = salesData;
        mContext = context;
        this.from = from;
    }

    @Override
    public BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.row_sales_item, parent, false);
        BH bh = new BH(v);
        Fonty.setFonts((ViewGroup) v);
        return bh;

    }

    @Override
    public void onBindViewHolder(BH holder, int position) {

        /*holder.id.setText(data.get(position).id);
        holder.date.setText(data.get(position).datetime);
        holder.reciveorder.setText(data.get(position).restaurantId);
        holder.unreciveorder.setText(data.get(position).customerName);
        holder.date.setText(data.get(position).subTotal + "SAR");*/

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class BH extends RecyclerView.ViewHolder {
        @BindView(R.id.id)
        TextView id;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.reciveorder)
        TextView reciveorder;
        @BindView(R.id.unreciveorder)
        TextView unreciveorder;
        @BindView(R.id.total)
        TextView total;

        BH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
