package com.hungry.oem.hungrysoluation.Model;

/**
 * Created by eng on 14/02/18.
 */

public class Messages {
    public int reply;
    public String message;

    public Messages(int reply, String message) {
        this.reply = reply;
        this.message =message;
    }
}
