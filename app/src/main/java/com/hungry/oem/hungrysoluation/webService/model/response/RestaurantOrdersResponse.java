package com.hungry.oem.hungrysoluation.webService.model.response;

import com.hungry.oem.hungrysoluation.Model.RestaurantOrderData;

import java.util.List;

/**
 * Created by oem on 11/4/18.
 */

public class RestaurantOrdersResponse extends CodeResponse{
    public List<RestaurantOrderData> data;
}
