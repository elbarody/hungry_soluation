package com.hungry.oem.hungrysoluation.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.view.Activity.DishDetailesActivity;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.models.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by eng on 12/02/18.
 */

public class MostPopularRecyclerViewAdapter extends RecyclerView.Adapter<MostPopularRecyclerViewAdapter.BH> {

    private static final int FOOTER_VIEW = 1;
    User mUser;

    private List<DishesData> data;
    private Context mContext;
    private Dialog dialog;
    private String from;
    private DishesData dishesData;

    public MostPopularRecyclerViewAdapter(List<DishesData> latestOffers, Context context, String from) {
        this.data = latestOffers;
        mContext = context;
        this.from = from;
    }

    @Override
    public BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.rv_item_most_popular, parent, false);
        BH bh = new BH(v);
        Fonty.setFonts((ViewGroup) v);
        return bh;

    }

    @Override
    public void onBindViewHolder(BH holder, int position) {
        holder.itemView.setOnClickListener(v -> {
            // Toast.makeText(mContext,""+position,Toast.LENGTH_LONG).show();
            dishesData = data.get(position);
            Intent intent = new Intent(mContext, DishDetailesActivity.class)
                    .putExtra("data",  dishesData);
            mContext.startActivity(intent);
        });
        Glide.with(mContext).load(D.IMAGE_URL + data.get(position).imageUrl).error(R.drawable.background2).into(holder.logo);
        holder.name.setText(data.get(position).name);
        holder.priceTv.setText(data.get(position).price + "SR");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class BH extends RecyclerView.ViewHolder {
        @BindView(R.id.logo)
        ImageView logo;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.price_tv)
        TextView priceTv;
        BH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
