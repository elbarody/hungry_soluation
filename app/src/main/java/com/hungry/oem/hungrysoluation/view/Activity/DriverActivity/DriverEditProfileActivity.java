package com.hungry.oem.hungrysoluation.view.Activity.DriverActivity;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.view.Activity.EditProfileActivity;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.UpdateCustmerRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.driver.UpdateDriverRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionDriverManager;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.Driver;
import com.xwady.core.models.User;

import java.io.ByteArrayOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverEditProfileActivity extends AppCompatActivity {
    @BindView(R.id.ed_car_licence)
    EditText edCarLicence;
    @BindView(R.id.ed_driver_licence)
    EditText edDriverLicence;
    private Bitmap bitmap;
    private ByteArrayOutputStream byteArrayOutputStream;
    private String[] encodedImage = {"", ""};
    @BindView(R.id.ed_username)
    EditText edUsername;
    @BindView(R.id.ed_email)
    EditText edEmail;
    @BindView(R.id.ed_pass)
    EditText edPass;
    @BindView(R.id.ed_rpass)
    EditText edRpass;
    @BindView(R.id.ed_car_type)
    EditText edCarType;
    @BindView(R.id.ed_car_model)
    EditText edCarModel;
    @BindView(R.id.ed_mobile)
    EditText edMobile;
    @BindView(R.id.ed_neighbourhood)
    EditText edNeighbourhood;
    @BindView(R.id.ed_city)
    EditText edCity;
    SessionDriverManager sessionDriverManager;
    Driver driver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_edit_profile);
        ButterKnife.bind(this);

        sessionDriverManager = new SessionDriverManager(this);
        driver = sessionDriverManager.getDriverDetails();

        edCarLicence.setText(driver.vehicleLicense);
        edCarModel.setText(driver.vehicleModel);
        edCarType.setText(driver.vehicleType);
        edCity.setText(driver.city);
        edDriverLicence.setText(driver.driverLicense);
        edEmail.setText(driver.email);
        edMobile.setText(driver.mobile);
        edNeighbourhood.setText(driver.district);
        edPass.setText(driver.password);
        edRpass.setText(driver.password);
        edUsername.setText(driver.userName);

        Fonty.setFonts(this);

    }

    @OnClick(R.id.btn_update)
    public void onViewClicked() {
        getUpdateRetrofit();
    }

    private void getUpdateRetrofit() {
        if (Utils.isValidEmail(edEmail) && Utils.isCarbonCopy(edPass, edRpass, getString(R.string.pass_not_match))) {
            //loader.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(D.DRIVER_URL).updateDriverData(new UpdateDriverRequest(driver.id, edUsername.getText().toString(), edCarLicence.getText().toString(), edCarType.getText().toString(), edCarModel.getText().toString(),
                    edDriverLicence.getText().toString(), edMobile.getText().toString(), edEmail.getText().toString(), edCity.getText().toString()
                    , "", "", edUsername.getText().toString(), edPass.getText().toString())).enqueue(new Callback<CodeResponse>() {
                @Override
                public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                    if (null == response.body()) {
                        onFailure(call, new Throwable());
                        return;
                    } else if (response.body().errorCode.equals("0")) {


                        DriverEditProfileActivity.this.finish();
                        //loader.setVisibility(View.GONE);
                    } else
                        Utils.showSnackbar(DriverEditProfileActivity.this, response.body().message);
                    //loader.setVisibility(View.GONE);

                }

                @Override
                public void onFailure(Call<CodeResponse> call, Throwable t) {
                    // loader.setVisibility(View.GONE);
                    Utils.showSnackbar(DriverEditProfileActivity.this, R.string.no_internet_connection);
                }
            });
        }

    }
}
