package com.hungry.oem.hungrysoluation.view.Activity;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.ContactUsRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.ContactUsResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountactUsActivity extends AppCompatActivity {

    @BindView(R.id.loader)
    MKLoader loader;
    @BindView(R.id.textView9)
    TextView textView9;
    @BindView(R.id.textView8)
    TextView textView8;
    @BindView(R.id.textView5)
    TextView textView5;
    @BindView(R.id.textView6)
    TextView textView6;

    @BindView(R.id.editText2)
    EditText editText2;
    @BindView(R.id.editText3)
    EditText editText3;
    @BindView(R.id.editText4)
    EditText editText4;
    @BindView(R.id.editText5)
    EditText editText5;

    User user;
    SessionManager sessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countact_us);
        ButterKnife.bind(this);
        sessionManager=new SessionManager(this);
        user=sessionManager.getUserDetails();
        editText2.setText(user.userName);
        editText3.setText(user.email);
        getContactUsInfo();
        Fonty.setFonts(this);

    }


    private void intiateDialog() {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);

        dialog.show();


        dialog.setCanceledOnTouchOutside(true);

        new Handler().postDelayed(() -> {
            dialog.dismiss();

        }, 2000);

    }
    private void getContactUsInfo() {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.BASE_URL).getContactUs(user.lang).enqueue(new Callback<ContactUsResponse>() {
            @Override
            public void onResponse(Call<ContactUsResponse> call, Response<ContactUsResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (response.body().errorCode.equals("0")) {
                    textView9.setText(response.body().data.title);
                    textView8.setText(response.body().data.address);
                    textView5.setText(response.body().data.info_email + "\n" + response.body().data.support_emai);
                    textView6.setText(response.body().data.mobile1 + "\n" + response.body().data.mobile2);
                }else
                    Utils.showSnackbar(CountactUsActivity.this,response.body().message);
                loader.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ContactUsResponse> call, Throwable t) {
                Utils.showSnackbar(CountactUsActivity.this,R.string.no_internet_connection);
                loader.setVisibility(View.GONE);
            }
        });
    }


    @OnClick(R.id.btn_send)
    public void onViewClicked() {
        createContactUsAPi();
    }

    private void createContactUsAPi() {
        if (Utils.isValidEmail(editText3)&&!Utils.isEditTextsEmpty(editText2,editText4,editText5)){
            loader.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(D.BASE_URL).createContactUs(new ContactUsRequest(editText2.getText().toString(),editText3.getText().toString(),editText4.getText().toString(),editText5.getText().toString()),user.lang).enqueue(new Callback<CodeResponse>() {
                @Override
                public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                    if (null == response.body()) {
                        onFailure(call, new Throwable());
                        return;
                    } else if (response.body().errorCode.equals("0")) {
                        intiateDialog();
                        loader.setVisibility(View.GONE);

                        //Utils.showSnackbar(CountactUsActivity.this,R.string.submit);
                    }else {
                        Utils.showSnackbar(CountactUsActivity.this, response.body().message);
                        loader.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<CodeResponse> call, Throwable t) {
                    Utils.showSnackbar(CountactUsActivity.this,R.string.no_internet_connection);
                    loader.setVisibility(View.GONE);
                }
            });
        }

    }
}
