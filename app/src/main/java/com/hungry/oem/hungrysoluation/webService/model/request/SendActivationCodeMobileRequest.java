package com.hungry.oem.hungrysoluation.webService.model.request;

/**
 * Created by oem on 11/11/18.
 */

public class SendActivationCodeMobileRequest {
    String EmailOrMobile;

    public SendActivationCodeMobileRequest(String emailOrMobile) {
        EmailOrMobile = emailOrMobile;
    }
}
