package com.hungry.oem.hungrysoluation.webService;


import com.hungry.oem.hungrysoluation.webService.model.request.ContactUsRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.DriverAcceptRequst;
import com.hungry.oem.hungrysoluation.webService.model.request.GetMessageRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.LoginRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.OrderRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.RateRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.RegistrationRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.SaveLocationRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.SendActivationCodeMobileRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.SendActivationCodeRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.SendMessageRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.SetDriverRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.UpdateCustmerRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.driver.LoginDriverRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.driver.RegistrationDriverRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.driver.UpdateDriverRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.restaurant.AddDishRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.restaurant.RestaurantRegistrationRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.AboutUsResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.BestSalesResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.ContactUsResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.DishesResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.ForgetPassResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.GetDriverOrderResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.GetMessagesResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.LoginResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.MarkerResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.RestaurantLoginResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.RestaurantOrdersResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.RestaurantResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.SetDriverResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.ShowReviewsResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.WelcomeResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.driver.LoginDriverResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.driver.SalesResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by barody on 4/11/18.
 */

public interface RetrofitService {

    @GET("welecom/get.php")
    Call<WelcomeResponse>getWelcome();

    @POST("login.php")
    Call<LoginResponse> createLogin(@Body LoginRequest request, @Query("Lang") String s);

    @POST("register.php")
    Call<CodeResponse> createRegistration(@Body RegistrationRequest request, @Query("Lang") String s);

    @POST("register.php")
    Call<CodeResponse> createRestaurantRegistration(@Body RestaurantRegistrationRequest request, @Query("Lang") String s);

    @POST("register.php")
    Call<CodeResponse> createRegistrationDriver(@Body RegistrationDriverRequest request, @Query("Lang") String s);

    @GET("getnearest.php")
    Call<RestaurantResponse> getNearestRestaurant(@Query("lang") String lang, @Query("Longitude") double Longitude, @Query("Latitude") double Latitude, @Query("miles") double miles);

    @GET("offers.php")
    Call<DishesResponse> getOffers(@Query("lang") String lang);

    @GET("offers.php")
    Call<DishesResponse> getOffers(@Query("searchTxt") String s, @Query("lang") String lang);


    @GET("getall.php")
    Call<DishesResponse> getAllDishes(@Query("Lang") String s);

    @GET("search.php")
    Call<DishesResponse> getDishesBySearchHome(@Query("searchTxt") String s, @Query("Longitude") double Longitude, @Query("Latitude") double Latitude);

    @GET("getall.php")
    Call<DishesResponse> getAllDishesFiltered(@Query("filter") String filter, @Query("Lang") String s);

    @GET("getall.php")
    Call<DishesResponse> getAllDishesSearch(@Query("searchTxt") String search, @Query("Lang") String s);

    @GET("get_by_resturant_id.php")
    Call<DishesResponse> getAllDishesFilteredForRestaurant(@Query("filter") String filter, @Query("restaurantId") String id, @Query("Lang") String s);

    @GET("get_by_resturant_id.php")
    Call<DishesResponse> getAllDishesSearchForRestaurant(@Query("searchTxt") String search, @Query("restaurantId") String id, @Query("Lang") String s);

    @POST("update.php")
    Call<CodeResponse> updateCustmerData(@Body UpdateCustmerRequest request, @Query("Lang") String s);

    @GET("get_most_popular.php")
    Call<DishesResponse> getMostPopular(@Query("Lang") String s);

    @GET("getall.php")
    Call<RestaurantResponse> getAllRestaurant(@Query("Lang") String s);

    @GET("getall.php")
    Call<RestaurantResponse> getRestaurantWithSearch(@Query("searchTxt") String s, @Query("Lang") String lang);

    /*@GET("get_by_resturant_id.php")
    Call<DishesResponse> getRestaurantDishes(@Query("restaurantId") String restaurantId, @Query("filter") String filter);
*/
    @GET("get_by_resturant_id.php")
    Call<DishesResponse> getRestaurantDishes(@Query("restaurantId") String restaurantId, @Query("Lang") String s);

    @GET("aboutus/get.php")
    Call<AboutUsResponse> getAboutusData(@Query("Lang") String s);

    @PUT("forgetPassword.php")
    Call<ForgetPassResponse> SendForgetPass(@Query("EmailOrMobile") String EmailOrMobile, @Query("Lang") String s);

    @GET("getfree.php")
    Call<MarkerResponse> getMarkerMap(@Query("Latitude") double lat, @Query("Longitude") double longa);

    @POST("order/setDriver.php")
    Call<SetDriverResponse> getSetDriver(@Body SetDriverRequest setDriverRequest);

    @GET("order/getDriverOrders.php")
    Call<GetDriverOrderResponse> getDriverOrders(@Query("driverId") int driverId);

    @POST("/order/driverAccept.php")
    Call<SetDriverResponse> getDriverAccept(@Body DriverAcceptRequst driverAcceptRequst);


    @POST("sendActivationCode.php")
    Call<CodeResponse> getActivitionCode(@Body SendActivationCodeMobileRequest request, @Query("Lang") String s);

    @POST("activateMobile.php")
    Call<CodeResponse> sendActivitionCode(@Body SendActivationCodeRequest request, @Query("Lang") String s);

    @GET("contactus/info.php")
    Call<ContactUsResponse> getContactUs(@Query("Lang") String s);

    @POST("contactus/create.php")
    Call<CodeResponse> createContactUs(@Body ContactUsRequest request, @Query("Lang") String s);

    @POST("create.php")
    Call<CodeResponse> createOrder(@Body OrderRequest request, @Query("lang") String Lang);

    @POST("restaurantReview/create.php")
    Call<CodeResponse> createRate(@Body RateRequest request, @Query("lang") String Lang);

    @POST("get_messages.php")
    Call<GetMessagesResponse> getMessages(@Body GetMessageRequest request, @Query("lang") String Lang);

    @POST("send_message.php")
    Call<CodeResponse> sendMessage(@Body SendMessageRequest request, @Query("lang") String Lang);

    //-------------------------------------------------------------------//
    //driver api
    @POST("register.php")
    Call<CodeResponse> RegistrationDriver(@Body RegistrationDriverRequest request, @Query("lang") String Lang);

    @POST("login.php")
    Call<LoginDriverResponse> createDriverLogin(@Body LoginDriverRequest request, @Query("lang") String Lang);

    @POST("update.php")
    Call<CodeResponse> updateDriverData(@Body UpdateDriverRequest request);

    @PUT("driver/forgetPassword.php")
    Call<ForgetPassResponse> SendDriverForgetPass(@Query("EmailOrMobile") String EmailOrMobile);


    @GET("getall.php")
    Call<SalesResponse> getAllSales(@Query("fromdate") String fromdate,
                                    @Query("todate") String todate,
                                    @Query("driverId") String driverId, @Query("lang") String Lang);

    @GET("getTotalOrders.php")
    Call<SalesResponse> getAllOrdersByTypeForRestaurant(@Query("type") String type, @Query("restaurantId") String restaurant_id, @Query("lang") String Lang);

    @GET("getall.php")
    Call<RestaurantOrdersResponse> getAllOrdersForRestaurant(@Query("restaurantId") String restaurant_id, @Query("lang") String Lang);

    @GET("getall.php")
    Call<RestaurantOrdersResponse> getAllOrdersForUser(@Query("customerId") String customerId, @Query("lang") String Lang);

    //Call<ForgetCodeResponse>ForgetCodeDriverResponse(@Query("EmailOrMobile") String code);

    @GET("restaurantReview/getall.php")
    Call<ShowReviewsResponse> getAllRates(@Query("restaurantId") String restaurantId, @Query("lang") String Lang);

    @POST("login.php")
    Call<RestaurantLoginResponse> createLoginRestaurant(@Body LoginRequest request, @Query("Lang") String s);


    @POST("create.php")
    Call<CodeResponse> createDishForRestaurant(@Body AddDishRequest request, @Query("lang") String Lang);


    @POST("updateLocation.php")
    Call<CodeResponse> saveMyLocation(@Body SaveLocationRequest request);

    @GET("getBestSales.php")
    Call<BestSalesResponse> getBestSalesForRestaurant(@Query("type") String from, @Query("restaurantId") String id, @Query("lang") String lang);

    @GET("order/getall.php")
    Call<GetDriverOrderResponse>getDriverLastOrders(@Query("driverId") int driverId,@Query("set")int i);
}
