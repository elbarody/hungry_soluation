package com.hungry.oem.hungrysoluation.webService.model.request;

/**
 * Created by oem on 9/19/18.
 */

public class RegistrationRequest {
    private String userName,email,password,mobile,lang="en";

    public RegistrationRequest(String userName, String email, String password, String mobile) {
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.mobile = mobile;
    }
}
