package com.hungry.oem.hungrysoluation.view.Activity;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.view.fragment.DishesFragment;
import com.hungry.oem.hungrysoluation.view.fragment.HomeFragment;
import com.hungry.oem.hungrysoluation.view.fragment.OffersFragment;
import com.hungry.oem.hungrysoluation.view.fragment.RestaurantFragment;
import com.hungry.oem.hungrysoluation.view.fragment.SettingFragment;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.SessionDriverManager;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.models.Driver;
import com.xwady.core.models.User;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements LocationListener {
    private static final int PERMISSIONS_REQUEST_LOCATION = 100;

    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private HomeFragment homeFragment;
    private RestaurantFragment restaurantFragment;
    private SettingFragment settingFragment;
    private OffersFragment offersFragment;
    private DishesFragment dishesFragment;
    protected LocationManager locationManager;
    public static ArrayList<DishesData> card_list = new ArrayList<>() ;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    homeFragment = new HomeFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fragment_container, homeFragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_restaurant:
                    restaurantFragment = new RestaurantFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fragment_container, restaurantFragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_dishes:
                    dishesFragment = new DishesFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fragment_container, dishesFragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_setting:
                    settingFragment = new SettingFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fragment_container, settingFragment);
                    transaction.commit();
                    return true;
                case R.id.navigation_offers:
                    offersFragment = new OffersFragment();
                    transaction = fragmentManager.beginTransaction();
                    transaction.replace(R.id.fragment_container, offersFragment);
                    transaction.commit();
                    return true;
            }
            return false;
        }
    };
    private User user;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sessionManager=new SessionManager(this);
        user=sessionManager.getUserDetails();

        fragmentManager = getSupportFragmentManager();
        homeFragment = new HomeFragment();
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, homeFragment);
        transaction.commit();

        if (isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION))
            runService();
        else
            requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, PERMISSIONS_REQUEST_LOCATION);

        statusCheck();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Fonty.setFonts(this);

    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.enable_gps)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, (dialog, id) -> startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNegativeButton(R.string.no, (dialog, id) -> dialog.cancel());
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean isPermissionGranted(String permission) {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, permission);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    private void requestPermission(String permission, int code) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{permission}, code);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0 &&grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                runService();
            } else {
                Toast.makeText(this, "Sorry!!! Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void runService() {

        locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 10, this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    @Override
    public void onLocationChanged(Location location) {
        user.lon = location.getLongitude();
        user.lat = location.getLatitude();
        sessionManager.createLoginSession(user);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
