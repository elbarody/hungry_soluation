package com.hungry.oem.hungrysoluation.webService.model.response;

import com.hungry.oem.hungrysoluation.Model.RatesData;

import java.util.List;

/**
 * Created by oem on 11/1/18.
 */

public class ShowReviewsResponse extends CodeResponse {
    public List<RatesData> data;
}
