package com.hungry.oem.hungrysoluation.view.Activity.restaurantActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.restaurant.RestaurantRegistrationRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationRestaurantActivity extends AppCompatActivity implements LocationListener {
    private static final int PERMISSIONS_REQUEST_LOCATION = 100;
    protected LocationManager locationManager;

    private static final int READ_STORAGE_CODE = 1001;
    private static final int WRITE_STORAGE_CODE = 1002;
    @BindView(R.id.user_name_et)
    EditText userNameEt;
    @BindView(R.id.constraintLayout_image)
    ConstraintLayout constraintLayoutImage;
    @BindView(R.id.image_fill)
    ImageView imageFill;
    @BindView(R.id.image_attach)
    ImageView imageAttach;
    private String[] encodedImage = {"", ""};
    private Bitmap bitmap;
    private ByteArrayOutputStream byteArrayOutputStream;

    @BindView(R.id.loader)
    MKLoader loader;
    @BindView(R.id.n_pass)
    EditText nPass;
    @BindView(R.id.new_pass)
    EditText newPass;
    @BindView(R.id.image_add)
    ImageView imageAdd;
    @BindView(R.id.owner_name_et)
    EditText ownerNameEt;
    @BindView(R.id.restaurant_name_et)
    EditText restaurantNameEt;
    @BindView(R.id.restaurant_email_et)
    EditText restaurantEmailEt;
    @BindView(R.id.restaurant_address)
    EditText restaurantAddress;
    @BindView(R.id.phone_et)
    EditText phoneEt;
    @BindView(R.id.city_et)
    EditText cityEt;
    @BindView(R.id.text_attach_fill)
    TextView textAttachFill;
    private SessionManager sessionManager;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_restaurant);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        if (isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION))
            runService();
        else
            requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, PERMISSIONS_REQUEST_LOCATION);

        statusCheck();
        Fonty.setFonts(this);

    }

    @OnClick({R.id.image_add, R.id.text_attach, R.id.btn_registration, R.id.image_attach})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_add:
                openImagePickerIntent(1001);
                break;
            case R.id.image_attach:
            case R.id.text_attach:
                openImagePickerIntent(1002);
                break;
            case R.id.btn_registration:
                registerRetrofit();
                break;
        }
    }


    private void registerRetrofit() {
        if (!Utils.isEditTextsEmpty(ownerNameEt, cityEt, userNameEt, phoneEt, restaurantNameEt, newPass, nPass) &&
                Utils.isValidEmail(restaurantEmailEt) && Utils.isCarbonCopy(newPass, nPass, String.valueOf(R.string.pass_not_match))) {
            loader.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(D.RESTAURANT_URL).createRestaurantRegistration(
                    new RestaurantRegistrationRequest(ownerNameEt.getText().toString(), restaurantNameEt.getText().toString(), encodedImage[1],
                            restaurantEmailEt.getText().toString(), phoneEt.getText().toString(), userNameEt.getText().toString(),
                            newPass.getText().toString(), restaurantAddress.getText().toString(), encodedImage[0], Double.toString(user.lon), Double.toString(user.lat)), user.lang)
                    .enqueue(new Callback<CodeResponse>() {
                        @Override
                        public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                            if (null == response.body()) {
                                onFailure(call, new Throwable());
                                return;
                            } else if (response.body().errorCode.equals("0")) {
                                intiateDialog();
                                loader.setVisibility(View.GONE);

                            } else {
                                intiateDailogCodeError(response.body().message);
                                loader.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onFailure(Call<CodeResponse> call, Throwable t) {
                            Utils.showSnackbar(RegistrationRestaurantActivity.this, R.string.no_internet_connection);
                            loader.setVisibility(View.GONE);
                        }
                    });
        }


    }

    private void intiateDailogCodeError(String message) {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);

        ImageView imageView = dialog.findViewById(R.id.image_checked);
        TextView textView = dialog.findViewById(R.id.dialog_tv);
        textView.setText(message);
        Glide.with(this).load("").error(R.drawable.ic_danger).into(imageView);
        dialog.show();

        dialog.setCanceledOnTouchOutside(true);

        new Handler().postDelayed(() -> {
            dialog.dismiss();
        }, 3000);
    }

    private void intiateDialog() {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);

        dialog.show();
        dialog.setOnDismissListener(dialog1 ->
                RegistrationRestaurantActivity.this.finish()
        );

        dialog.setCanceledOnTouchOutside(true);

        new Handler().postDelayed(() -> {
            dialog.dismiss();
            RegistrationRestaurantActivity.this.finish();

        }, 3000);

    }

    private void runService() {

        locationManager = (LocationManager) getSystemService(this.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 10, this);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (resultCode == -1 && (requestCode == 1001 || requestCode == 1002)) {
            Uri selectedImage = imageReturnedIntent.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
            byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream); //bitmap is the bitmap object
            byte[] byteArrayImage = byteArrayOutputStream.toByteArray();
            if (requestCode == 1001) {
                Glide.with(RegistrationRestaurantActivity.this).load(selectedImage).into(imageFill);
                encodedImage[0] = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
            }
            if (requestCode == 1002) {
                Glide.with(RegistrationRestaurantActivity.this).load(selectedImage).into(imageAttach);
                //textAttachFill.setText(selectedImage.getPath());
                encodedImage[1] = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
            }
        }
    }

    private void openImagePickerIntent(int PICK_IMAGE) {
        if (isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, PICK_IMAGE);
        } else {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, READ_STORAGE_CODE);
        }
    }

    private boolean isPermissionGranted(String permission) {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, permission);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.enable_gps)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, (dialog, id) -> startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNegativeButton(R.string.no, (dialog, id) -> dialog.cancel());
        final AlertDialog alert = builder.create();
        alert.show();
    }
    //Requesting permission
    private void requestPermission(String permission, int code) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{permission}, code);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                runService();
            } else {
                Toast.makeText(this, "Sorry!!! Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
        //Checking the request code of our request
        if (requestCode == READ_STORAGE_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                openImagePickerIntent(1001);


            } else {

                RegistrationRestaurantActivity.this.finish();
            }
        } else if (requestCode == WRITE_STORAGE_CODE) {


            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {

                RegistrationRestaurantActivity.this.finish();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        user.lon = location.getLongitude();
        user.lat = location.getLatitude();
        sessionManager.createLoginSession(user);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
