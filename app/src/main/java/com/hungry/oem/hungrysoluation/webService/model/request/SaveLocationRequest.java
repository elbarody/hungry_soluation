package com.hungry.oem.hungrysoluation.webService.model.request;

public class SaveLocationRequest {
    private String id,Longitude, Latitude;

    public SaveLocationRequest(String id, String Longitude, String Latitude) {
        this.id = id;
        this.Longitude = Longitude;
        this.Latitude = Latitude;
    }
}
