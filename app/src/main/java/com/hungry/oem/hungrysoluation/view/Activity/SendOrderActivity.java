package com.hungry.oem.hungrysoluation.view.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.OrderRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.addressInfo;
import com.hungry.oem.hungrysoluation.webService.model.request.items;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.CheckBoxCore;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendOrderActivity extends AppCompatActivity {

    @BindView(R.id.fname)
    EditText ed_fname;
    @BindView(R.id.lname)
    EditText ed_lname;
    @BindView(R.id.company_name)
    EditText ed_company_name;
    @BindView(R.id.ed_country)
    EditText ed_country;
    @BindView(R.id.ed_addressnum)
    EditText ed_address_num;
    @BindView(R.id.ed_address)
    EditText ed_address;
    @BindView(R.id.ed_city)
    EditText ed_city;
    @BindView(R.id.ed_postcode)
    EditText ed_postcode;
    @BindView(R.id.ed_phonee)
    EditText ed_phone;
    @BindView(R.id.ed_email)
    EditText ed_email;
    @BindView(R.id.ed_state)
    EditText ed_state;
    @BindView(R.id.subtotal)
    TextView subtotal;
    @BindView(R.id.shipping)
    TextView shipping;
    @BindView(R.id.total)
    TextView total;
    @BindView(R.id.read)
    CheckBox ck_read;
    @BindView(R.id.btn_send_order)
    Button btn_send_order;
    SessionManager sessionManager;
    User user;
    String Datee = "";
    addressInfo addressInfo;
    ArrayList<items> items;
    Unbinder unbinder;
    @BindView(R.id.back_image)
    ImageView back_image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_order);
        unbinder = ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        Date currentTime = Calendar.getInstance().getTime();
        Datee = String.valueOf(currentTime);

        ed_phone.setText(user.phone);
        ed_email.setText(user.email);

        btn_send_order.setOnClickListener(view -> {
            if (CheckBoxCore.RequriedOneCheckBox(view, getString(R.string.checkbox_message), ck_read)) {

                addressInfo = new addressInfo(user.id, ed_fname.getText().toString(), ed_lname.getText().toString(), ed_company_name.getText().toString(),
                        ed_country.getText().toString(), ed_address_num.getText().toString() + "" + ed_address.getText().toString(), ed_city.getText().toString(), ed_postcode.getText().toString(), ed_phone.getText().toString()
                        , ed_email.getText().toString());
                items = new ArrayList<>();
                for (int i = 0; i < MainActivity.card_list.size(); i++) {
                    items items1 = new items(MainActivity.card_list.get(i).id, MainActivity.card_list.get(i).mainPrice, ("" + MainActivity.card_list.get(i).count), ("" + MainActivity.card_list.get(i).restaurantId));
                    items.add(items1);
                }
                SendOrder(addressInfo, items);


            }
        });


        back_image.setOnClickListener(view -> SendOrderActivity.this.finish());

        Fonty.setFonts(this);

    }


    private void SendOrder(addressInfo addressInfo, ArrayList<items> items) {
        if (Utils.isValidEmail(ed_email) && !Utils.isEditTextsEmpty(ed_fname, ed_lname, ed_company_name
                , ed_address_num, ed_address, ed_city, ed_phone, ed_email)) {
            //loader.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(D.ORDER_URL).createOrder(new OrderRequest(user.id, Datee, subtotal.getText().toString(), shipping.getText().toString(), addressInfo, items),user.lang).enqueue(new Callback<CodeResponse>() {
                @Override
                public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                    if (null == response.body()) {
                        onFailure(call, new Throwable());
                        return;
                    } else if (response.body().errorCode.equals("0")) {
                        Utils.showSnackbar(SendOrderActivity.this, R.string.submit);
                        MainActivity.card_list.clear();
                        intiateDialog();

                    } else
                        Utils.showSnackbar(SendOrderActivity.this, response.body().message);
                    //loader.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<CodeResponse> call, Throwable t) {
                    Utils.showSnackbar(SendOrderActivity.this, R.string.no_internet_connection);
                    //loader.setVisibility(View.GONE);
                }
            });
        }

    }

    private void intiateDialog() {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        new Handler().postDelayed(() -> {

            startActivity(new Intent(SendOrderActivity.this,MainActivity.class ));
            SendOrderActivity.this.finish();
            dialog.dismiss();

        }, 2000);

    }


}
