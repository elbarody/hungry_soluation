package com.hungry.oem.hungrysoluation.view.fragment.fragmentRestaurant;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;

import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.DishRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.view.Activity.ItemInCartActivity;
import com.hungry.oem.hungrysoluation.view.Activity.restaurantActivity.AddDishActivity;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.DishesResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DishesForRestaurantFragment extends Fragment {


    @BindView(R.id.rc_dishes)
    RecyclerView rvDishes;
    Unbinder unbinder;

    private DishRecyclerViewAdapter dishRecyclerViewAdapter;
    List<DishesData> dishesData = new ArrayList<>();

    User user;
    SessionManager sessionManager;
    private String filter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dishes_for_restaurant, container, false);
        unbinder = ButterKnife.bind(this, view);

        sessionManager = new SessionManager(getActivity());
        user = sessionManager.getUserDetails();
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(view1 ->
                startActivity(new Intent(getActivity(), AddDishActivity.class))
        );
        dishRecyclerViewAdapter = new DishRecyclerViewAdapter(dishesData, getContext(), "restaurant");
        rvDishes.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvDishes.setAdapter(dishRecyclerViewAdapter);
        rvDishes.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });


        getDishesRetrofit();
        Fonty.setFonts((ViewGroup) view);

        return view;
    }

    private void intiateDialog() {
        Dialog dialog = new Dialog(getContext(), R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_dishes);
        ImageView popular = dialog.findViewById(R.id.image_propularity);
        ImageView newness = dialog.findViewById(R.id.image_newness);
        ImageView average = dialog.findViewById(R.id.image_average);
        ImageView lowToHigh = dialog.findViewById(R.id.image_low_to_hight);
        ImageView highToLow = dialog.findViewById(R.id.image_high_to_low);

        dialog.show();
        popular.setOnClickListener(v -> {
            filter = "moreLovely";
            getFilterDishesRetrofit();
            dialog.dismiss();
        });

        newness.setOnClickListener(v -> {
            filter = "recently";
            getFilterDishesRetrofit();
            dialog.dismiss();
        });

        average.setOnClickListener(v -> {
            filter = "avg";
            getFilterDishesRetrofit();
            dialog.dismiss();
        });

        lowToHigh.setOnClickListener(v -> {
            filter = "asc";
            getFilterDishesRetrofit();
            dialog.dismiss();
        });

        highToLow.setOnClickListener(v -> {
            filter = "desc";
            getFilterDishesRetrofit();
            dialog.dismiss();
        });
        dialog.setCanceledOnTouchOutside(true);


        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
    }

    private void getFilterDishesRetrofit() {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getAllDishesFilteredForRestaurant(filter,user.id,user.lang).enqueue(new Callback<DishesResponse>() {
            @Override
            public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    dishesData.clear();
                    dishesData.addAll(response.body().data);
                    dishRecyclerViewAdapter.notifyDataSetChanged();
                }
                //loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<DishesResponse> call, Throwable t) {
                Utils.showSnackbar(getActivity(), R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

    private void getDishesRetrofit() {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getRestaurantDishes(user.id,user.lang).enqueue(new Callback<DishesResponse>() {
            @Override
            public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    dishesData.addAll(response.body().data);
                    dishRecyclerViewAdapter.notifyDataSetChanged();
                    //loader.setVisibility(View.GONE);
                }
                    //loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<DishesResponse> call, Throwable t) {
                Utils.showSnackbar(getActivity(), R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

    private void intiateDialogSearch() {
        Dialog dialog = new Dialog(getContext(), R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_search);
        ImageView search = dialog.findViewById(R.id.image_search);
        EditText editText = dialog.findViewById(R.id.search_et);

        dialog.show();
        editText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                getSearchDishesRetrofit(editText.getText().toString());
                dialog.dismiss();
                return true;
            }
            return false;
        });

        search.setOnClickListener(v -> {
            //filter = "desc";
            getSearchDishesRetrofit(editText.getText().toString());
            dialog.dismiss();
        });
        dialog.setCanceledOnTouchOutside(true);


        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
    }

    private void getSearchDishesRetrofit(String search) {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getAllDishesSearchForRestaurant(search, user.id, user.lang)
                .enqueue(new Callback<DishesResponse>() {
                    @Override
                    public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                        if (null == response.body()) {
                            onFailure(call, new Throwable());
                            return;
                        } else if (!response.body().data.isEmpty()) {
                            dishesData.clear();
                            dishesData.addAll(response.body().data);
                            dishRecyclerViewAdapter.notifyDataSetChanged();
                        }
                        //loader.setVisibility(View.GONE);

                    }

                    @Override
                    public void onFailure(Call<DishesResponse> call, Throwable t) {
                        Utils.showSnackbar(getActivity(), R.string.no_internet_connection);
                        //loader.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.cart_image, R.id.deliver_image, R.id.filter_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cart_image:
                Intent intent = new Intent(DishesForRestaurantFragment.this.getContext(), ItemInCartActivity.class);
                startActivity(intent);
                break;
            case R.id.deliver_image:
                intiateDialogSearch();
                break;
            case R.id.filter_image:
                intiateDialog();
                break;
        }
    }
}
