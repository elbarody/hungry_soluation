package com.hungry.oem.hungrysoluation.view.Activity;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.UpdateCustmerRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    @BindView(R.id.ed_name)
    EditText edName;
    @BindView(R.id.ed_phone)
    EditText edPhone;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.ed_pass)
    EditText edPass;
    @BindView(R.id.ed_rpass)
    EditText edRpass;
    @BindView(R.id.ed_r_n_pass)
    EditText edRNPass;
    @BindView(R.id.loader)
    MKLoader loader;
    private SessionManager sessionManager;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);

        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();

        edName.setText(user.userName);
        edPhone.setText(user.phone);
        etEmail.setText(user.email);
        Fonty.setFonts(this);

    }

    @OnClick(R.id.btn_register)
    public void onViewClicked() {
        getUpdateRetrofit();
    }

    private void getUpdateRetrofit() {
        if (!user.password.equals(edPass.getText().toString())) {
            Utils.showSnackbar(this, R.string.password_not_corect);
            return;
        }
        if (!Utils.isEditTextsEmpty(edName, edPass, edPhone, edRNPass, etEmail) && Utils.isValidEmail(etEmail) && Utils.isCarbonCopy(edRNPass, edRpass, getString(R.string.pass_not_match))) {
            loader.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(D.USER_URL).updateCustmerData(new UpdateCustmerRequest(user.id, edName.getText().toString(), etEmail.getText().toString(), edRNPass.getText().toString(), edPhone.getText().toString(), edPass.getText().toString()),user.lang)
                    .enqueue(new Callback<CodeResponse>() {
                        @Override
                        public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                            if (null == response.body()) {
                                onFailure(call, new Throwable());
                                return;
                            } else if (response.body().errorCode.equals("0")) {
                                user.userName = edName.getText().toString();
                                user.email = etEmail.getText().toString();
                                user.phone = edPhone.getText().toString();
                                sessionManager.createLoginSession(user);

                                //user.password = edRNPass.getText().toString();
                                if (!edRNPass.getText().toString().equals(user.password))
                                    sessionManager.logoutUser(ChooseActivity.class);

                                intiateDialog();
                                loader.setVisibility(View.GONE);
                            } else
                                Utils.showSnackbar(EditProfileActivity.this, response.body().message);
                            loader.setVisibility(View.GONE);

                        }

                        @Override
                        public void onFailure(Call<CodeResponse> call, Throwable t) {
                            loader.setVisibility(View.GONE);
                            Utils.showSnackbar(EditProfileActivity.this, R.string.no_internet_connection);
                        }
                    });
        }

    }

    private void intiateDialog() {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);

        dialog.show();
        dialog.setOnDismissListener(dialog1 ->
                EditProfileActivity.this.finish()
        );

        dialog.setCanceledOnTouchOutside(true);

        new Handler().postDelayed(() -> {
            dialog.dismiss();
            EditProfileActivity.this.finish();

        }, 2000);

    }
}
