package com.hungry.oem.hungrysoluation.view.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.model.Step;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.models.User;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, DirectionCallback {
    SessionManager sessionManager;
    private GoogleMap googleMap;
    private MapView mMapView;
    private String serverKey = "AIzaSyB7NYc-6s5M741uOZ3fi95FkrjCWBdt6pU";
    private LatLng origin ;         /*= new LatLng(37.7849569, -122.4068855);*/
    private LatLng destination;     /* = new LatLng(37.7849444, -122.4068444);*/
    private float lat, lon;
    private User user;
    private String request_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        request_id=getIntent().getStringExtra("request_id");
        //getProviderLocation();
        lat = getIntent().getFloatExtra("lat", 0.0f);
        lon = getIntent().getFloatExtra("lon", 0.0f);


        origin = new LatLng(lat, lon);

        //getProviderLocation();
        mMapView = (MapView) findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        try {
            MapsInitializer.initialize(this.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(googleMap -> MapActivity.this.googleMap = googleMap);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }



    public void requestDirection() {
        GoogleDirection.withServerKey(serverKey)
                .from(origin)
                .to(destination)
                .transportMode(TransportMode.DRIVING)
                .execute(this);
    }


    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        if (direction.isOK()) {
            Route route = direction.getRouteList().get(0);
            int legCount = route.getLegList().size();
            for (int index = 0; index < legCount; index++) {
                Leg leg = route.getLegList().get(index);
                googleMap.addMarker(new MarkerOptions().position(leg.getStartLocation().getCoordination()));
                if (index == legCount - 1) {
                    googleMap.addMarker(new MarkerOptions().position(leg.getEndLocation().getCoordination()));
                }
                List<Step> stepList = leg.getStepList();
                ArrayList<PolylineOptions> polylineOptionList = DirectionConverter.createTransitPolyline(this, stepList, 5, Color.RED, 3, Color.BLUE);
                for (PolylineOptions polylineOption : polylineOptionList) {
                    googleMap.addPolyline(polylineOption);
                }
            }
            setCameraWithCoordinationBounds(route);

        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {
    }

    private void setCameraWithCoordinationBounds(Route route) {
        LatLng southwest = route.getBound().getSouthwestCoordination().getCoordination();
        LatLng northeast = route.getBound().getNortheastCoordination().getCoordination();
        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }

    /*private void getProviderLocation() {
        RetrofitWebService.getService(D.DRIVER_URL).get(user.token,request_id).enqueue(new Callback<ProviderLocationResponse>() {
            @Override
            public void onResponse(Call<ProviderLocationResponse> call, Response<ProviderLocationResponse> response) {
                if (null==response.body()||response.body().code==400){
                    onFailure(call,new Throwable());
                    return;
                }else if (response.body().code==0){
                    destination=new LatLng(response.body().data.lat,response.body().data.lon);
                    requestDirection();
                }
            }

            @Override
            public void onFailure(Call<ProviderLocationResponse> call, Throwable t) {

            }
        });
    }*/


}
