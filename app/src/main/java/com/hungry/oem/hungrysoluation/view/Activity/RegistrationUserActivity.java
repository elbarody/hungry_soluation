package com.hungry.oem.hungrysoluation.view.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.RegistrationRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.marcinorlowski.fonty.Fonty;
import com.rilixtech.CountryCodePicker;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationUserActivity extends AppCompatActivity {

    @BindView(R.id.ed_name)
    EditText edName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.ed_pass)
    EditText edPass;
    @BindView(R.id.ed_rpass)
    EditText edRpass;
    @BindView(R.id.loader)
    MKLoader loader;
    @BindView(R.id.ed_phone)
    EditText edPhone;

    User user;
    SessionManager sessionManager;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    private String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_user);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        ButterKnife.bind(this);
        Fonty.setFonts(this);

    }

    @OnClick(R.id.btn_register)
    public void onViewClicked() {
        createRetrofit();
    }

    private void createRetrofit() {

        phone =  "+"+ccp.getSelectedCountryCode() + edPhone.getText().toString();
        if (!Utils.isEditTextsEmpty(edName, edPass, edPhone, etEmail) && Utils.isValidEmail(etEmail) && Utils.isCarbonCopy(edPass, edRpass, getString(R.string.pass_not_match))) {
            loader.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(D.USER_URL).createRegistration(new RegistrationRequest(edName.getText().toString(), etEmail.getText().toString(), edPass.getText().toString(), phone), user.lang)
                    .enqueue(new Callback<CodeResponse>() {
                        @Override
                        public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                            if (null == response.body()) {
                                onFailure(call, new Throwable());
                                return;
                            } else if (response.body().errorCode.equals("0")) {
                                intiateDialog();
                                loader.setVisibility(View.GONE);
                            } else if (response.body().errorCode.equals("3")) {
                                intiateDialog();
                                loader.setVisibility(View.GONE);
                            } else {
                                intiateDailogCodeError(response.body().message);
                                loader.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onFailure(Call<CodeResponse> call, Throwable t) {
                            Utils.showSnackbar(RegistrationUserActivity.this, R.string.no_internet_connection);
                            loader.setVisibility(View.GONE);
                        }
                    });
        }

        loader.setVisibility(View.GONE);

    }

    private void intiateDialog() {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);

        dialog.show();
        dialog.setOnDismissListener(dialog1 ->
                RegistrationUserActivity.this.finish()
        );

        dialog.setCanceledOnTouchOutside(true);

        new Handler().postDelayed(() -> {
            dialog.dismiss();
            startActivity(new Intent(RegistrationUserActivity.this, Code_ForgetPass_Activity.class).putExtra("phone", phone));
            RegistrationUserActivity.this.finish();

        }, 3000);

    }

    private void intiateDailogCodeError(String message) {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);

        ImageView imageView = dialog.findViewById(R.id.image_checked);
        TextView textView = dialog.findViewById(R.id.dialog_tv);
        textView.setText(message);
        Glide.with(this).load("").error(R.drawable.ic_danger).into(imageView);
        dialog.show();

        dialog.setCanceledOnTouchOutside(true);

        new Handler().postDelayed(() -> {
            dialog.dismiss();
        }, 3000);
    }
}
