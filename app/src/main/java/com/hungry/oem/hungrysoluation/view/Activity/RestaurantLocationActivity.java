package com.hungry.oem.hungrysoluation.view.Activity;

import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hungry.oem.hungrysoluation.Model.MapDataOrder;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.DriverAcceptRequst;
import com.hungry.oem.hungrysoluation.webService.model.response.GetDriverOrderResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.SetDriverResponse;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionDriverManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.Driver;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantLocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    SupportMapFragment map;
    GoogleMap googleMap;
    int ordeId = 0;
    Button button;
    MKLoader loader;
    private Driver driver;
    private SessionDriverManager sessionDriverManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (googleServicesAvailable()) {
            setContentView(R.layout.activity_driver_location);
            sessionDriverManager=new SessionDriverManager(this);
            driver=sessionDriverManager.getDriverDetails();
            map = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            button = findViewById(R.id.button);
            loader = findViewById(R.id.loader);
            initMap();
            button.setEnabled(false);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    driverAccept(loader);
                }
            });

        }
    }


    private void initMap() {
        mapMarker(loader);
        map.getMapAsync(this);
    }


    private void goToLocation(GoogleMap map, LatLng ll, MapDataOrder data) {
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 15f);
        map.moveCamera(update);
        map.addMarker(new MarkerOptions()
                .anchor(0.0f, 1.0f)
                .position(ll).snippet("sdfdsf"));

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                ordeId = data.id;
                button.setEnabled(true);
                return true;

            }
        });
    }

    private Boolean googleServicesAvailable()

    {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int isAvailable = apiAvailability.isGooglePlayServicesAvailable(
                RestaurantLocationActivity.this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (apiAvailability.isUserResolvableError(isAvailable)) {
            Dialog dialog = apiAvailability.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(this, getString(R.string.cant_connect_play_service)
                    , Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private void mapMarker(MKLoader loader) {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.BASE_URL).getDriverLastOrders(Integer.parseInt(driver.id),1)
                .enqueue(new Callback<GetDriverOrderResponse>() {
                    @Override
                    public void onResponse(Call<GetDriverOrderResponse> call,
                                           Response<GetDriverOrderResponse> response) {
                        if (null == response.body()) {
                            onFailure(call, new Throwable());
                            return;
                        } else if (response.body().errorCode.equals("0")) {
                            loader.setVisibility(View.GONE);
                            for (MapDataOrder i : response.body().data) {
                                LatLng ll = new LatLng(Double.parseDouble(i.Latitude_rest),
                                        Double.parseDouble(i.Longitude_rest));
                                goToLocation(googleMap, ll, i);
                            }
                        } else {
                            Utils.showSnackbar(RestaurantLocationActivity.this, response.body().
                                    message);
                            loader.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<GetDriverOrderResponse> call, Throwable t) {
                        Utils.showSnackbar(RestaurantLocationActivity.this,
                                R.string.no_internet_connection);
                        loader.setVisibility(View.GONE);
                    }

                });
    }

    private void driverAccept(MKLoader loader) {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.BASE_URL).getDriverAccept(new DriverAcceptRequst(Integer.toString(ordeId), "4"))
                .enqueue(new Callback<SetDriverResponse>() {
                    @Override
                    public void onResponse(Call<SetDriverResponse> call,
                                           Response<SetDriverResponse> response) {
                        if (null == response.body()) {
                            onFailure(call, new Throwable());
                            return;
                        } else if (response.body().errorCode.equals("0")) {
                            loader.setVisibility(View.GONE);
                            Toast.makeText(RestaurantLocationActivity.this,
                                    response.body().message, Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            loader.setVisibility(View.GONE);
                            Toast.makeText(RestaurantLocationActivity.this,
                                    response.body().message, Toast.LENGTH_LONG).show();
                            finish();
                            Utils.showSnackbar(RestaurantLocationActivity.this, response.body().
                                    message);
                            loader.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<SetDriverResponse> call, Throwable t) {
                        Utils.showSnackbar(RestaurantLocationActivity.this,
                                R.string.no_internet_connection);
                        loader.setVisibility(View.GONE);
                    }

                });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        if (ContextCompat.checkSelfPermission(RestaurantLocationActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(RestaurantLocationActivity.this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            return;

        }
        googleMap.setMyLocationEnabled(true);
    }
}
