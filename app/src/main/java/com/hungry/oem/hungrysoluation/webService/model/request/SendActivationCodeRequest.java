package com.hungry.oem.hungrysoluation.webService.model.request;

/**
 * Created by oem on 11/11/18.
 */

public class SendActivationCodeRequest {
    public SendActivationCodeRequest(String mobile, String activationCode) {
        this.mobile = mobile;
        this.activationCode = activationCode;
    }

    private String mobile, activationCode;
}
