package com.hungry.oem.hungrysoluation.webService.model.request.restaurant;

/**
 * Created by oem on 11/1/18.
 */

public class RestaurantRegistrationRequest {
    private String responsible, name, license,
            email, mobile,userName, password,
            address, logoImgeURL, Longitude, Latitude;

    public RestaurantRegistrationRequest(String responsible, String name, String license, String email, String mobile, String userName, String password, String address, String logoImgeURL, String longitude, String latitude) {
        this.responsible = responsible;
        this.name = name;
        this.license = license;
        this.email = email;
        this.mobile = mobile;
        this.userName = userName;
        this.password = password;
        this.address = address;
        this.logoImgeURL = logoImgeURL;
        Longitude = longitude;
        Latitude = latitude;
    }
}
