package com.hungry.oem.hungrysoluation.webService.model.response;

import com.hungry.oem.hungrysoluation.Model.ContactUsData;

/**
 * Created by oem on 10/17/18.
 */

public class ContactUsResponse extends CodeResponse {
    public ContactUsData data;
}
