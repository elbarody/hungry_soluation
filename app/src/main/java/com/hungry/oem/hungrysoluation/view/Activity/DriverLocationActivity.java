package com.hungry.oem.hungrysoluation.view.Activity;

import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hungry.oem.hungrysoluation.Model.MapData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.SetDriverRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.MarkerResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.SetDriverResponse;
import com.makeramen.roundedimageview.RoundedImageView;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverLocationActivity extends AppCompatActivity
        implements OnMapReadyCallback {

    String orderId = "0", driverId = "0", resturantId = "0";
    SupportMapFragment map;
    GoogleMap googleMap;
    Boolean flag = false;
    Button button;
    MKLoader loader;
    ConstraintLayout constrain;
    RoundedImageView personPhoto;
    MaterialRatingBar ratingBar;
    TextView userName;
    private SessionManager sessionManager;
    private User user;
    private Marker marker;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        if (googleServicesAvailable()) {
            setContentView(R.layout.activity_restaurant_location);
            userName = findViewById(R.id.userName);
            ratingBar = findViewById(R.id.rate);
            map = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            orderId = getIntent().getStringExtra("orderId");
            button = findViewById(R.id.button);
            loader = findViewById(R.id.loader);
            constrain = findViewById(R.id.constrain);
            personPhoto = findViewById(R.id.personPhoto);
            initMap();
            button.setOnClickListener(v -> {
                startOrder(loader);
                Toast.makeText(DriverLocationActivity.this, resturantId + orderId, Toast.LENGTH_LONG).show();
            });

        }
    }


    private void initMap() {
        mapMarker(loader, "driver/");
        map.getMapAsync(this);
    }


    private void goToLocation(GoogleMap map, LatLng ll, List<MapData> data) {
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 8f);
        map.moveCamera(update);

        map.setOnMarkerClickListener(marker -> {
            int position = (int) marker.getTag();
            constrain.setVisibility(View.VISIBLE);
            userName.setText(data.get(position).name);
            ratingBar.setRating(data.get(position).rate);
            driverId= String.valueOf(data.get(position).id);
            Glide.with(DriverLocationActivity.this).load(R.drawable.secound_start).into(personPhoto);
            return true;
        });

    }

    private Boolean googleServicesAvailable() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int isAvailable = apiAvailability.isGooglePlayServicesAvailable(DriverLocationActivity.this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (apiAvailability.isUserResolvableError(isAvailable)) {
            Dialog dialog = apiAvailability.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(this, getString(R.string.cant_connect_play_service), Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private void mapMarker(MKLoader loader, String url) {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.BASE_URL + url).getMarkerMap(user.lat, user.lon)
                .enqueue(new Callback<MarkerResponse>() {
                    @Override
                    public void onResponse(Call<MarkerResponse> call, Response<MarkerResponse> response) {
                        if (null == response.body()) {
                            onFailure(call, new Throwable());
                            return;
                        } else if (response.body().errorCode.equals("0")) {
                            loader.setVisibility(View.GONE);
                            for (int i = 0; i < response.body().data.size(); i++){
                                if (null == response.body().data.get(i).Latitude || null == response.body().data.get(i).Longitude){
                                    response.body().data.get(i).Latitude="0.0";
                                    response.body().data.get(i).Longitude="0.0";
                                }
                                LatLng ll = new LatLng(Double.parseDouble(response.body().data.get(i).Latitude), Double.parseDouble(response.body().data.get(i).Longitude));
                                marker = googleMap.addMarker(new MarkerOptions().position(ll).title(response.body().data.get(i).name).snippet(response.body().data.get(i).address));
                                marker.setTag(i);
                                goToLocation(googleMap, ll, response.body().data);
                            }
                        } else {
                            Utils.showSnackbar(DriverLocationActivity.this, response.body().message);
                            loader.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFailure(Call<MarkerResponse> call, Throwable t) {

                    }
                });
    }


    private void startOrder(MKLoader loader) {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.BASE_URL)
                .getSetDriver(new SetDriverRequest(orderId, user.id, driverId))
                .enqueue(new Callback<SetDriverResponse>() {
                    @Override
                    public void onResponse(Call<SetDriverResponse> call, Response<SetDriverResponse> response) {
                        if (null == response.body()) {
                            onFailure(call, new Throwable());
                            return;
                        } else if (response.body().errorCode.equals("0")) {
                            Toast.makeText(DriverLocationActivity.this,
                                    response.body().message, Toast.LENGTH_LONG).show();
                            finish();
                            loader.setVisibility(View.GONE);
                        } else {
                            Utils.showSnackbar(DriverLocationActivity.this,
                                    response.body().message);
                            loader.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onFailure(Call<SetDriverResponse> call, Throwable t) {
                        Utils.showSnackbar(DriverLocationActivity.this,
                                R.string.no_internet_connection);
                        loader.setVisibility(View.GONE);
                    }
                });

    }


    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        if (ContextCompat.checkSelfPermission(DriverLocationActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(DriverLocationActivity.this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            return;
        }
        map.setMyLocationEnabled(true);
    }
}