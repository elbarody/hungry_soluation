package com.hungry.oem.hungrysoluation.view.fragment;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;

import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.OfferesRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.view.Activity.ItemInCartActivity;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.DishesResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersFragment extends Fragment {

    @BindView(R.id.rc_offers)
    RecyclerView rvOffers;


    Unbinder unbinder;
    @BindView(R.id.loader)
    MKLoader loader;
    private OfferesRecyclerViewAdapter offeresRecyclerViewAdapter;
    List<DishesData> offersData = new ArrayList<>();
    private SessionManager sessionManager;
    private User user;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_offers, container, false);
        unbinder = ButterKnife.bind(this, view);
        sessionManager = new SessionManager(getActivity());
        user = sessionManager.getUserDetails();
        offeresRecyclerViewAdapter = new OfferesRecyclerViewAdapter(offersData, getContext(), "");
        rvOffers.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvOffers.setAdapter(offeresRecyclerViewAdapter);
        rvOffers.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        getDishesRetrofit();

        Fonty.setFonts((ViewGroup) view);

        return view;
    }

    private void getDishesRetrofit() {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getOffers(user.lang).enqueue(new Callback<DishesResponse>() {
            @Override
            public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    offersData.addAll(response.body().data);
                    offeresRecyclerViewAdapter.notifyDataSetChanged();
                    loader.setVisibility(View.GONE);

                }

            }

            @Override
            public void onFailure(Call<DishesResponse> call, Throwable t) {
                Utils.showSnackbar(getActivity(), R.string.no_internet_connection);
                loader.setVisibility(View.GONE);
            }
        });
    }

    private void intiateDialogSearch() {
        Dialog dialog = new Dialog(getContext(), R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_search);
        ImageView search = dialog.findViewById(R.id.image_search);
        EditText editText = dialog.findViewById(R.id.search_et);
        editText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                getDishesRetrofitBySearch(editText.getText().toString());
                dialog.dismiss();
                return true;
            }
            return false;
        });
        dialog.show();


        search.setOnClickListener(v -> {
            //filter = "desc";
            getDishesRetrofitBySearch(editText.getText().toString());
            dialog.dismiss();
        });
        dialog.setCanceledOnTouchOutside(true);


        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
    }

    private void getDishesRetrofitBySearch(String s) {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getOffers(s,user.lang).enqueue(new Callback<DishesResponse>() {
            @Override
            public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    offersData.clear();
                    offersData.addAll(response.body().data);
                    offeresRecyclerViewAdapter.notifyDataSetChanged();
                }
                loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<DishesResponse> call, Throwable t) {
                Utils.showSnackbar(getActivity(), R.string.no_internet_connection);
                loader.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.cart_image, R.id.deliver_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cart_image:
                Intent intent = new Intent(OffersFragment.this.getContext(), ItemInCartActivity.class);
                startActivity(intent);
                break;
            case R.id.deliver_image:
                intiateDialogSearch();
                break;
        }
    }
}
