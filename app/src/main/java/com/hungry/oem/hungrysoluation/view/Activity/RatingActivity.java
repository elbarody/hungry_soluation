package com.hungry.oem.hungrysoluation.view.Activity;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hungry.oem.hungrysoluation.R;
import com.makeramen.roundedimageview.RoundedImageView;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class RatingActivity extends AppCompatActivity {
    String message;
    TextView driveName;
    MaterialRatingBar rate;
    RoundedImageView personPhoto;
    Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        driveName = findViewById(R.id.userName);
        rate = findViewById(R.id.rate);
        personPhoto = findViewById(R.id.personPhoto);
        button = findViewById(R.id.button);

        driveName.setText(" Baroody");
        rate.setRating(3.4f);
        personPhoto.setBackground(ActivityCompat.getDrawable(RatingActivity.this,R.drawable.background_btn));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rate.setRating(4.0f);

            }
        });
    }
}
