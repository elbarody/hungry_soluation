package com.hungry.oem.hungrysoluation.view.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;

import com.hungry.oem.hungrysoluation.Model.RestaurantData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.ResturantsRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.view.Activity.ItemInCartActivity;
import com.hungry.oem.hungrysoluation.view.Activity.ResturantDishesActivity;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.DishesResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.RestaurantResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by oem on 9/17/18.
 */

public class RestaurantFragment extends Fragment {

    @BindView(R.id.rc_resturant)
    RecyclerView rvResturant;
    Unbinder unbinder;
    @BindView(R.id.loader)
    MKLoader loader;
    private ResturantsRecyclerViewAdapter resturantsRecyclerViewAdapter;

    List<RestaurantData> restaurantData = new ArrayList<>();
    private SessionManager sessionManager;
    private User user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_restaurant, container, false);
        unbinder = ButterKnife.bind(this, view);
        sessionManager = new SessionManager(getActivity());
        user = sessionManager.getUserDetails();
        resturantsRecyclerViewAdapter = new ResturantsRecyclerViewAdapter(restaurantData, getContext(), "");
        rvResturant.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvResturant.setAdapter(resturantsRecyclerViewAdapter);
        rvResturant.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        getNearestRestaurant();

        Fonty.setFonts((ViewGroup) view);

        return view;
    }

    private void getNearestRestaurant() {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.RESTAURANT_URL).getAllRestaurant(user.lang).enqueue(new Callback<RestaurantResponse>() {
            @Override
            public void onResponse(Call<RestaurantResponse> call, Response<RestaurantResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    restaurantData.addAll(response.body().data);
                    resturantsRecyclerViewAdapter.notifyDataSetChanged();
                    loader.setVisibility(View.GONE);
                }else
                    loader.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<RestaurantResponse> call, Throwable t) {
                Utils.showSnackbar(getActivity(), R.string.no_internet_connection);
                loader.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.cart_image, R.id.deliver_image, R.id.back_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cart_image:
                Intent intent = new Intent(RestaurantFragment.this.getContext(), ItemInCartActivity.class);
                startActivity(intent);
                break;
            case R.id.deliver_image:
                intiateDialogSearch();
                break;
            case R.id.back_image:
                break;
        }
    }

    private void intiateDialogSearch() {
        Dialog dialog = new Dialog(getActivity(), R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_search);
        ImageView search = dialog.findViewById(R.id.image_search);
        EditText editText=dialog.findViewById(R.id.search_et);
        dialog.show();
        editText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                getSearchDishesRetrofit(editText.getText().toString());
                dialog.dismiss();
                return true;
            }
            return false;
        });
        search.setOnClickListener(v -> {
            //filter = "desc";
            getSearchDishesRetrofit(editText.getText().toString());
            dialog.dismiss();
        });
        dialog.setCanceledOnTouchOutside(true);


        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
    }

    private void getSearchDishesRetrofit(String search) {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.RESTAURANT_URL).getRestaurantWithSearch(search,user.lang)
                .enqueue(new Callback<RestaurantResponse>() {
                    @Override
                    public void onResponse(Call<RestaurantResponse> call, Response<RestaurantResponse> response) {
                        if (null == response.body()) {
                            onFailure(call, new Throwable());
                            return;
                        } else if (!response.body().data.isEmpty()) {
                            restaurantData.clear();
                            restaurantData.addAll(response.body().data);
                            resturantsRecyclerViewAdapter.notifyDataSetChanged();
                        }
                        loader.setVisibility(View.GONE);

                    }

                    @Override
                    public void onFailure(Call<RestaurantResponse> call, Throwable t) {
                        Utils.showSnackbar(getActivity(), R.string.no_internet_connection);
                        loader.setVisibility(View.GONE);
                    }
                });
    }
}
