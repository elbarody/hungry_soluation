package com.hungry.oem.hungrysoluation.view.Activity.DriverActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.view.Activity.Code_ForgetPass_Activity;
import com.hungry.oem.hungrysoluation.view.Activity.RegistrationUserActivity;
import com.hungry.oem.hungrysoluation.view.Activity.restaurantActivity.RegistrationRestaurantActivity;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.driver.RegistrationDriverRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationDriverActivity extends AppCompatActivity {
    private static final int READ_STORAGE_CODE = 1001;
    private static final int WRITE_STORAGE_CODE = 1002;
    int PLACE_PICKER_REQUEST = 1;
    @BindView(R.id.ed_car_licence)
    EditText edCarLicence;
    @BindView(R.id.ed_driver_licence)
    EditText edDriverLicence;
    @BindView(R.id.image_attach_car)
    ImageView imageAttachCar;
    @BindView(R.id.image_attach_driver)
    ImageView imageAttachDriver;
    private Bitmap bitmap;
    private ByteArrayOutputStream byteArrayOutputStream;
    private String[] encodedImage = {"", ""};
    @BindView(R.id.ed_username)
    EditText edUsername;
    @BindView(R.id.ed_email)
    EditText edEmail;
    @BindView(R.id.ed_pass)
    EditText edPass;
    @BindView(R.id.ed_rpass)
    EditText edRpass;
    @BindView(R.id.ed_car_type)
    EditText edCarType;
    @BindView(R.id.ed_car_model)
    EditText edCarModel;
    @BindView(R.id.ed_mobile)
    EditText edMobile;
    @BindView(R.id.ed_neighbourhood)
    EditText edNeighbourhood;
    @BindView(R.id.ed_city)
    EditText edCity;
    @BindView(R.id.loader)
    MKLoader mkLoader;

    private static int RESULT_LOAD_IMAGE = 1;
    String picturePath = "";
    Bitmap image;
    MKLoader avi;


    private static int IMG_RESULT = 1;
    private User user;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_driver);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        Fonty.setFonts(this);

    }

    @OnClick({R.id.ed_car_licence, R.id.ed_driver_licence, R.id.btn_register,R.id.image_attach_car, R.id.image_attach_driver})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_attach_car:
            case R.id.ed_car_licence:
                openImagePickerIntent(1001);
                break;
            case R.id.image_attach_driver:
            case R.id.ed_driver_licence:
                openImagePickerIntent(1002);
                break;
            case R.id.btn_register:
                createDriverAPI(mkLoader);
                break;
        }
    }

    private void createDriverAPI(MKLoader mkLoader) {

        mkLoader.setVisibility(View.VISIBLE);
        if (Utils.isValidEmail(edEmail) && Utils.isCarbonCopy(edPass, edRpass, getString(R.string.pass_not_match))) {
            //loader.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(D.DRIVER_URL).RegistrationDriver(new RegistrationDriverRequest(edUsername.getText().toString(), edCarLicence.getText().toString(), edCarType.getText().toString(), edCarModel.getText().toString(),
                    edDriverLicence.getText().toString(), edMobile.getText().toString(), edEmail.getText().toString(), edCity.getText().toString()
                    , "", "", edUsername.getText().toString(), edPass.getText().toString()), user.lang)
                    .enqueue(new Callback<CodeResponse>() {
                        @Override
                        public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                            if (null == response.body()) {
                                onFailure(call, new Throwable());
                                mkLoader.setVisibility(View.GONE);
                                return;
                            } else if (response.body().errorCode.equals("0")) {
                                RegistrationDriverActivity.this.finish();
                                Utils.showSnackbar(RegistrationDriverActivity.this, response.body().message);
                                intiateDialog();
                                mkLoader.setVisibility(View.GONE);
                            } else {
                                intiateDailogCodeError(response.body().message);
                                mkLoader.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onFailure(Call<CodeResponse> call, Throwable t) {
                            Utils.showSnackbar(RegistrationDriverActivity.this, R.string.no_internet_connection);
                            mkLoader.setVisibility(View.GONE);
                        }
                    });
        } else {
            mkLoader.setVisibility(View.GONE);
        }

        //loader.setVisibility(View.VISIBLE);
    }

    private void intiateDailogCodeError(String message) {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);

        ImageView imageView = dialog.findViewById(R.id.image_checked);
        TextView textView = dialog.findViewById(R.id.dialog_tv);
        textView.setText(message);
        Glide.with(this).load("").error(R.drawable.ic_danger).into(imageView);
        dialog.show();

        dialog.setCanceledOnTouchOutside(true);

        new Handler().postDelayed(() -> {
            dialog.dismiss();
        }, 3000);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (resultCode == -1 && (requestCode == 1001 || requestCode == 1002)) {
            Uri selectedImage = imageReturnedIntent.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
            byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream); //bitmap is the bitmap object
            byte[] byteArrayImage = byteArrayOutputStream.toByteArray();
            if (requestCode == 1001) {
                //edCarLicence.setText(selectedImage.getPath());
                Glide.with(RegistrationDriverActivity.this).load(selectedImage).into(imageAttachCar);

                encodedImage[0] = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
            }
            if (requestCode == 1002) {
                Glide.with(RegistrationDriverActivity.this).load(selectedImage).into(imageAttachDriver);
                //edDriverLicence.setText(selectedImage.getPath());
                encodedImage[1] = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
            }
        }
    }

   /* @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (requestCode == IMG_RESULT && resultCode == RESULT_OK
                    && null != data) {


                Uri URI = data.getData();
                String[] FILE = {MediaStore.Images.Media.DATA};


                Cursor cursor = getContentResolver().query(URI,
                        FILE, null, null, null);

                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(FILE[0]);
                String Image = cursor.getString(columnIndex);


                //upload_profile_image.setImageBitmap(BitmapFactory.decodeFile(Image));


                cursor.close();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Please try again", Toast.LENGTH_LONG)
                    .show();
        }
        *//*if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
            //Cls_general.USER.User_Image = picturePath ;
            //image = BitmapFactory.decodeFile(picturePath);
            upload_profile_image.setImageBitmap(BitmapFactory.decodeFile(picturePath));*//*

    }*/

    private void openImagePickerIntent(int PICK_IMAGE) {


        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE);


    }

    private boolean isPermissionGranted(String permission) {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, permission);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }


    //Requesting permission
    private void requestPermission(String permission, int code) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{permission}, code);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == READ_STORAGE_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                openImagePickerIntent(1001);


            } else {

                RegistrationDriverActivity.this.finish();
            }
        } else if (requestCode == WRITE_STORAGE_CODE) {


            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {

                RegistrationDriverActivity.this.finish();
            }
        }
    }

    private void intiateDialog() {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);

        dialog.show();
        dialog.setOnDismissListener(dialog1 ->
                RegistrationDriverActivity.this.finish()
        );

        dialog.setCanceledOnTouchOutside(true);

        new Handler().postDelayed(() -> {
            dialog.dismiss();
            RegistrationDriverActivity.this.finish();

        }, 3000);

    }


}
