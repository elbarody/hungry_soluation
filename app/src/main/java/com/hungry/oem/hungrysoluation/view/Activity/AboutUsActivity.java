package com.hungry.oem.hungrysoluation.view.Activity;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.AboutUsResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.ContactUsResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutUsActivity extends AppCompatActivity {



    @BindView(R.id.about_title)
    TextView about_title;
    @BindView(R.id.about_body)
    TextView about_body;
    @BindView(R.id.why_title)
    TextView why_title;
    @BindView(R.id.why_body)
    TextView why_body;
    @BindView(R.id.worry_title)
    TextView worry_title;
    @BindView(R.id.worry_body)
    TextView worry_body;
    @BindView(R.id.how_title)
    TextView how_title;
    @BindView(R.id.how_body)
    TextView how_body;
    @BindView(R.id.loader)
    MKLoader loader;
    private SessionManager sessionManager;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        getAboutUsInfo(loader);
        Fonty.setFonts(this);

    }

    private void getAboutUsInfo(MKLoader loader) {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.BASE_URL).getAboutusData(user.lang).enqueue(new Callback<AboutUsResponse>() {
            @Override
            public void onResponse(Call<AboutUsResponse> call, Response<AboutUsResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (response.body().errorCode.equals("0")) {
                    about_title.setText(response.body().data.about_title);
                    about_body.setText(response.body().data.about);
                    why_title.setText(response.body().data.why_title);
                    why_body.setText(response.body().data.why_body);
                    worry_title.setText(response.body().data.worry_title);
                    worry_body.setText(response.body().data.worry_body);
                    how_title.setText(response.body().data.how_title);
                    how_body.setText(response.body().data.how_body);
                }else
                    Utils.showSnackbar(AboutUsActivity.this,response.body().message);
                    loader.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<AboutUsResponse> call, Throwable t) {
                Utils.showSnackbar(AboutUsActivity.this,R.string.no_internet_connection);
                loader.setVisibility(View.GONE);
            }
        });
    }
}
