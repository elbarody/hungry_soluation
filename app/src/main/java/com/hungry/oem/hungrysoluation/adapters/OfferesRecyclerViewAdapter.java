package com.hungry.oem.hungrysoluation.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.view.Activity.DishDetailesActivity;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.models.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OfferesRecyclerViewAdapter extends RecyclerView.Adapter<OfferesRecyclerViewAdapter.BH> {

    private static final int FOOTER_VIEW = 1;
    User mUser;

    private List<DishesData> data;
    private Context mContext;
    private Dialog dialog;
    private String from;
    private DishesData dishesData;

    public OfferesRecyclerViewAdapter(List<DishesData> latestOffers, Context context, String from) {
        this.data = latestOffers;
        mContext = context;
        this.from = from;
    }

    @Override
    public BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.offer_rec_row, parent, false);
        BH bh = new BH(v);
        Fonty.setFonts((ViewGroup) v);
        return bh;

    }

    @Override
    public void onBindViewHolder(BH holder, int position) {
        holder.button6.setOnClickListener(v -> {
            // Toast.makeText(mContext,""+position,Toast.LENGTH_LONG).show();
            dishesData = data.get(position);
            Intent intent = new Intent(mContext, DishDetailesActivity.class)
                    .putExtra("data",  dishesData);
            mContext.startActivity(intent);
        });
        Glide.with(mContext).load(D.IMAGE_URL + data.get(position).imageUrl).error(R.drawable.background2).into(holder.imageView3);
        holder.dishName.setText(data.get(position).name);
        holder.salary.setText(data.get(position).price + "SR");
        holder.textView27.setText(data.get(position).discount+"%");

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class BH extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView3)
        ImageView imageView3;
        @BindView(R.id.textView27)
        TextView textView27;
        @BindView(R.id.dish_name)
        TextView dishName;
        @BindView(R.id.salary)
        TextView salary;
        @BindView(R.id.button6)
        Button button6;
        BH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
