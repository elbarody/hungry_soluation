package com.hungry.oem.hungrysoluation.view.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.view.Activity.DriverActivity.MainActivityDriver;
import com.hungry.oem.hungrysoluation.view.Activity.DriverActivity.RegistrationDriverActivity;
import com.hungry.oem.hungrysoluation.view.Activity.restaurantActivity.MainRestaurantActivity;
import com.hungry.oem.hungrysoluation.view.Activity.restaurantActivity.RegistrationRestaurantActivity;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.LoginRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.SendActivationCodeMobileRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.driver.LoginDriverRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.LoginResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.RestaurantLoginResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.driver.LoginDriverResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionDriverManager;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.Driver;
import com.xwady.core.models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.loader)
    MKLoader loader;
    @BindView(R.id.facebook_btn)
    Button facebookBtn;
    @BindView(R.id.twitter_btn)
    Button twitterBtn;
    @BindView(R.id.google_id)
    Button googleId;
    private User user;
    private Driver driver;
    private SessionManager sessionManager;
    private SessionDriverManager sessionDriverManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        sessionManager = new SessionManager(this);
        sessionDriverManager = new SessionDriverManager(this);
        user = sessionManager.getUserDetails();
        driver = sessionDriverManager.getDriverDetails();
        if (!user.type.equals("user")) {
            googleId.setVisibility(View.GONE);
            facebookBtn.setVisibility(View.GONE);
            twitterBtn.setVisibility(View.GONE);
        }
        Fonty.setFonts(this);

    }

    @OnClick({R.id.forget_pass_tv, R.id.facebook_btn, R.id.twitter_btn, R.id.google_id, R.id.login_btn, R.id.create_btn})
    public void onViewClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.forget_pass_tv:
                intent = new Intent(view.getContext(), ForgetPassActivity.class);
                startActivity(intent);
                break;
            case R.id.facebook_btn:
                break;
            case R.id.twitter_btn:
                break;
            case R.id.google_id:
                break;
            case R.id.login_btn:
                if (user.type.equals("user")) {
                    loginRetrofit();
                } else if (user.type.equals("driver")) {
                    loginRetrofitDriver();
                    //startActivity(new Intent(this, MainActivityDriver.class));
                } else if (user.type.equals("restaurant")) {
                    loginRetrofitRestaurant();

                }
                break;
            case R.id.create_btn:
                if (user.type.equals("user")) {
                    intent = new Intent(view.getContext(), RegistrationUserActivity.class);
                    startActivity(intent);
                } else if (user.type.equals("driver")) {
                    startActivity(new Intent(this, RegistrationDriverActivity.class));
                } else if (user.type.equals("restaurant")) {
                    startActivity(new Intent(this, RegistrationRestaurantActivity.class));
                }
                break;
        }
    }

    private void loginRetrofitRestaurant() {
        if (!Utils.isEditTextsEmpty(etEmail, etPassword)) {
            loader.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(D.RESTAURANT_URL).createLoginRestaurant(new LoginRequest(etEmail.getText().toString(), etPassword.getText().toString()), user.lang)
                    .enqueue(new Callback<RestaurantLoginResponse>() {
                        @Override
                        public void onResponse(Call<RestaurantLoginResponse> call, Response<RestaurantLoginResponse> response) {
                            if (null == response.body()) {
                                onFailure(call, new Throwable());
                                return;
                            } else if (response.body().errorCode.equals("0")) {
                                user.id = response.body().data.id;
                                user.userName = response.body().data.userName;
                                user.email = response.body().data.email;
                                user.phone = response.body().data.mobile;
                                user.password = etPassword.getText().toString();
                                user.address = response.body().data.address;
                                user.license = response.body().data.license;
                                user.img = response.body().data.logoImgeURL;
                                user.lat = Double.parseDouble(response.body().data.Latitude);
                                user.lon = Double.parseDouble(response.body().data.Longitude);
                                user.delivery_fee = response.body().data.deliveryFee;
                                user.setLoginStatus(User.AccountStatus.LOGGED_IN_RESTAURANT);
                                sessionManager.createLoginSession(user);
                                startActivity(new Intent(LoginActivity.this, MainRestaurantActivity.class));
                                LoginActivity.this.finish();
                                loader.setVisibility(View.GONE);
                            } else {
                                intiateDailogCodeError(response.body().message);
                                loader.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onFailure(Call<RestaurantLoginResponse> call, Throwable t) {
                            loader.setVisibility(View.GONE);
                            Utils.showSnackbar(LoginActivity.this, R.string.no_internet_connection);
                        }
                    });
        }
    }

    private void loginRetrofit() {
        if (!Utils.isEditTextsEmpty(etEmail, etPassword)) {
            loader.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(D.USER_URL).createLogin(new LoginRequest(etEmail.getText().toString(), etPassword.getText().toString()), user.lang)
                    .enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            if (null == response.body()) {
                                onFailure(call, new Throwable());
                                return;
                            } else if (response.body().errorCode.equals("0")) {
                                user.id = response.body().data.id;
                                user.userName = response.body().data.userName;
                                user.email = response.body().data.email;
                                user.phone = response.body().data.mobile;
                                user.password = etPassword.getText().toString();
                                user.setLoginStatus(User.AccountStatus.LOGGED_IN_USER);
                                sessionManager.createLoginSession(user);
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                LoginActivity.this.finish();
                                loader.setVisibility(View.GONE);
                            } else if (response.body().errorCode.equals("3")) {
                                getActivationCode("customer/", response.body().data.mobile);
                                loader.setVisibility(View.GONE);
                            } else {
                                intiateDailogCodeError(response.body().message);
                                loader.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            loader.setVisibility(View.GONE);
                            Utils.showSnackbar(LoginActivity.this, R.string.no_internet_connection);
                        }
                    });
        }
    }

    private void intiateDailogCodeError(String message) {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);

        ImageView imageView = dialog.findViewById(R.id.image_checked);
        TextView textView = dialog.findViewById(R.id.dialog_tv);
        textView.setText(message);
        Glide.with(this).load("").error(R.drawable.ic_danger).into(imageView);
        dialog.show();

        dialog.setCanceledOnTouchOutside(true);

        new Handler().postDelayed(() -> {
            dialog.dismiss();
        }, 3000);
    }

    public void getActivationCode(String type, String phone) {
        RetrofitWebService.getService(D.BASE_URL + type).getActivitionCode(new SendActivationCodeMobileRequest(phone), user.lang).
                enqueue(new Callback<CodeResponse>() {
                    @Override
                    public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                        if (null == response.body()) {
                            onFailure(call, new Throwable());
                            return;
                        } else if (response.body().errorCode.equals("0")) {
                            startActivity(new Intent(LoginActivity.this, Code_ForgetPass_Activity.class).putExtra("phone", phone));
                            Utils.showSnackbar(LoginActivity.this, response.body().message);

                        }
                    }

                    @Override
                    public void onFailure(Call<CodeResponse> call, Throwable t) {

                    }
                });
    }


    private void loginRetrofitDriver() {
        if (!Utils.isEditTextsEmpty(etEmail, etPassword)) {
            loader.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(D.DRIVER_URL).createDriverLogin(new LoginDriverRequest(etEmail.getText().toString(), etPassword.getText().toString()), user.lang)
                    .enqueue(new Callback<LoginDriverResponse>() {
                        @Override
                        public void onResponse(Call<LoginDriverResponse> call, Response<LoginDriverResponse> response) {
                            if (null == response.body()) {
                                onFailure(call, new Throwable());
                                return;
                            } else if (response.body().errorCode.equals("0")) {
                                driver.id = response.body().data.id;
                                driver.address = response.body().data.address;
                                driver.email = response.body().data.email;
                                driver.district = response.body().data.district;
                                driver.driverLicense = response.body().data.driverLicense;
                                driver.mobile = response.body().data.mobile;
                                driver.userName = response.body().data.userName;
                                driver.vehicleLicense = response.body().data.vehicleLicense;
                                driver.vehicleModel = response.body().data.vehicleModel;
                                driver.vehicleType = response.body().data.vehicleType;
                                driver.setLoginStatus(Driver.AccountStatus.LOGGED_IN_DRIVER);

                                sessionDriverManager.createLoginSession(driver);
                                startActivity(new Intent(LoginActivity.this, MainActivityDriver.class));
                                LoginActivity.this.finish();
                                loader.setVisibility(View.GONE);
                            } else {
                                intiateDailogCodeError(response.body().message);
                                loader.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onFailure(Call<LoginDriverResponse> call, Throwable t) {
                            loader.setVisibility(View.GONE);
                            Utils.showSnackbar(LoginActivity.this, R.string.no_internet_connection);
                        }
                    });
        }
    }



}
