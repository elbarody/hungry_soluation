package com.hungry.oem.hungrysoluation.webService.model.response;

import com.hungry.oem.hungrysoluation.Model.BestSaleData;

import java.util.List;

public class BestSalesResponse extends CodeResponse{
    public List<BestSaleData> data;
}
