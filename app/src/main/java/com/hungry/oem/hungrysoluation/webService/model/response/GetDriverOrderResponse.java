package com.hungry.oem.hungrysoluation.webService.model.response;

import com.hungry.oem.hungrysoluation.Model.MapDataOrder;

import java.util.ArrayList;

public class GetDriverOrderResponse extends CodeResponse {

    public ArrayList<MapDataOrder> data;
}

