package com.hungry.oem.hungrysoluation.webService.model.response;


import com.hungry.oem.hungrysoluation.Model.Messages;

import java.util.List;

/**
 * Created by eng on 27/02/18.
 */

public class GetMessagesResponse extends CodeResponse {
    public List<Messages> data;
}
