package com.hungry.oem.hungrysoluation.webService.model.request.restaurant;

/**
 * Created by oem on 11/4/18.
 */

public class AddDishRequest {
    private String restaurantId,name,descText,price,discount,imageUrl;

    public AddDishRequest(String restaurantId, String name, String descText, String price, String discount, String imageUrl) {
        this.restaurantId = restaurantId;
        this.name = name;
        this.descText = descText;
        this.price = price;
        this.discount = discount;
        this.imageUrl = imageUrl;
    }
}
