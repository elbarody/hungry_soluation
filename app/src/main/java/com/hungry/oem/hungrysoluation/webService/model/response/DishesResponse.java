package com.hungry.oem.hungrysoluation.webService.model.response;

import com.hungry.oem.hungrysoluation.Model.DishesData;

import java.util.List;

/**
 * Created by oem on 10/8/18.
 */

public class DishesResponse extends CodeResponse {
    public List<DishesData> data;
}
