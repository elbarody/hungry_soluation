package com.hungry.oem.hungrysoluation.view.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.ImageView;

import com.hungry.oem.hungrysoluation.Model.Messages;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.MessagesRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.GetMessageRequest;
import com.hungry.oem.hungrysoluation.webService.model.request.SendMessageRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.GetMessagesResponse;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.EndlessRecyclerViewScrollListener;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.LinearLayout.VERTICAL;

public class MessagesActivity extends AppCompatActivity {

    @BindView(R.id.messages_rv)
    RecyclerView messagesRv;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_new_message)
    EditText etNewMessage;
    @BindView(R.id.imageView11)
    ImageView imageView11;
    private MessagesRecyclerViewAdapter messagesRecyclerViewAdapter;
    private List<Messages> messages = new ArrayList<>();
    private User user;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(v -> startActivity(new Intent(MessagesActivity.this, MainActivity.class)));
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, VERTICAL, true) {

            @Override
            public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
                LinearSmoothScroller smoothScroller = new LinearSmoothScroller(MessagesActivity.this) {

                    private static final float SPEED = 300f;

                    @Override
                    protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                        return SPEED / displayMetrics.densityDpi;
                    }

                };
                smoothScroller.setTargetPosition(position);
                startSmoothScroll(smoothScroller);
            }

        };
        messagesRv.setLayoutManager(layoutManager);
        messagesRecyclerViewAdapter = new MessagesRecyclerViewAdapter(messages, this);
        messagesRv.setHasFixedSize(true);
        messagesRv.setAdapter(messagesRecyclerViewAdapter);
        messagesRv.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        //getMessagesAPI();
        Fonty.setFonts(this);


    }

    private void getMessagesAPI() {

        RetrofitWebService.getService(D.BASE_URL).getMessages(new GetMessageRequest(user.token),user.lang).enqueue(new Callback<GetMessagesResponse>() {
            @Override
            public void onResponse(Call<GetMessagesResponse> call, Response<GetMessagesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (response.body().equals("0")) {
                    messages.addAll(response.body().data);
                    messagesRecyclerViewAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<GetMessagesResponse> call, Throwable t) {
                Utils.showSnackbar(MessagesActivity.this, R.string.no_internet_connection);

            }
        });
    }


    @OnClick(R.id.imageView11)
    public void onClick() {
        //sendMessageAPI();
        messagesRecyclerViewAdapter.addItem(etNewMessage.getText().toString());
        messagesRecyclerViewAdapter.notifyItemInserted(0);
        messagesRv.scrollToPosition(0);
        etNewMessage.setText("");
    }

    private void sendMessageAPI() {
        if (!Utils.isEditTextsEmpty(etNewMessage)) {
            imageView11.setClickable(false);
            RetrofitWebService.getService(D.BASE_URL).sendMessage(new SendMessageRequest(user.token, etNewMessage.getText().toString()),user.lang)
                    .enqueue(new Callback<CodeResponse>() {
                        @Override
                        public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                            if (null == response.body()) {
                                onFailure(call, new Throwable());
                                return;
                            } else if (response.body().errorCode.equals("0")) {
                                messagesRecyclerViewAdapter.addItem(etNewMessage.getText().toString());
                                messagesRecyclerViewAdapter.notifyItemInserted(0);
                                messagesRv.scrollToPosition(0);
                                etNewMessage.setText("");
                                imageView11.setClickable(true);
                            }
                        }

                        @Override
                        public void onFailure(Call<CodeResponse> call, Throwable t) {
                            Utils.showSnackbar(MessagesActivity.this, R.string.no_internet_connection);

                        }
                    });

        }
        //Utils.showSnackbar(this,R.string.empity_message);
    }
}
