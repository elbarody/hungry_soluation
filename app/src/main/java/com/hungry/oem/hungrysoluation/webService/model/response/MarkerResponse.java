package com.hungry.oem.hungrysoluation.webService.model.response;

import com.hungry.oem.hungrysoluation.Model.MapData;

import java.util.ArrayList;
import java.util.List;

public class MarkerResponse extends CodeResponse {
    public List<MapData> data;
}
