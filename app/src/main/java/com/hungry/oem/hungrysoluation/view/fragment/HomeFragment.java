package com.hungry.oem.hungrysoluation.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.Model.RestaurantData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.DishesToOrderRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.adapters.HomeRestaurantRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.adapters.MostPopularRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.view.Activity.ItemInCartActivity;
import com.hungry.oem.hungrysoluation.view.Activity.OrdersDetailsForUserActivity;
import com.hungry.oem.hungrysoluation.view.Activity.SeeMoreActivity;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.DishesResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.RestaurantResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {
    @BindView(R.id.rv_most_popular)
    RecyclerView rvMostPopular;
    @BindView(R.id.rv_top_rest)
    RecyclerView rvTopRest;
    @BindView(R.id.rv_dishes)
    RecyclerView rvDishes;
    Unbinder unbinder;
    @BindView(R.id.loader)
    MKLoader loader;
    @BindView(R.id.search_et)
    EditText searchEt;
    private MostPopularRecyclerViewAdapter mostPopularRecyclerViewAdapter;
    private DishesToOrderRecyclerViewAdapter dishesToOrderRecyclerViewAdapter;
    private HomeRestaurantRecyclerViewAdapter homeResturantRecyclerViewAdapter;

    private List<RestaurantData> restautantData = new ArrayList<>();
    private List<DishesData> dishesData = new ArrayList<>();
    private List<DishesData> mostPopular = new ArrayList<>();
    private SessionManager sessionManager;
    private User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.fragment_home, null);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);

        sessionManager = new SessionManager(getActivity());
        user = sessionManager.getUserDetails();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mostPopularRecyclerViewAdapter = new MostPopularRecyclerViewAdapter(mostPopular, getActivity(), "");
        rvMostPopular.setLayoutManager(layoutManager);
        rvMostPopular.setAdapter(mostPopularRecyclerViewAdapter);
        rvMostPopular.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        homeResturantRecyclerViewAdapter = new HomeRestaurantRecyclerViewAdapter(restautantData, getActivity(), "");
        rvTopRest.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvTopRest.setAdapter(homeResturantRecyclerViewAdapter);
        rvTopRest.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        dishesToOrderRecyclerViewAdapter = new DishesToOrderRecyclerViewAdapter(dishesData, getActivity(), "");
        rvDishes.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvDishes.setAdapter(dishesToOrderRecyclerViewAdapter);
        rvDishes.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        Fonty.setFonts((ViewGroup) view);

        getNearestRestaurant();
        getDishesRetrofit();
        getMostPopular();

        searchEt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                startActivity(new Intent(getActivity(), SeeMoreActivity.class).putExtra("from", "search").putExtra("text", searchEt.getText().toString()));
                return true;
            }
            return false;
        });

        return view;
    }

    private void getDishesRetrofit() {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getAllDishes(user.lang).enqueue(new Callback<DishesResponse>() {
            @Override
            public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    dishesData.addAll(response.body().data);
                    dishesToOrderRecyclerViewAdapter.notifyDataSetChanged();
                }
                //loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<DishesResponse> call, Throwable t) {
                Utils.showSnackbar(getActivity(), R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

    private void getMostPopular() {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getMostPopular(user.lang).enqueue(new Callback<DishesResponse>() {
            @Override
            public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    mostPopular.addAll(response.body().data);
                    mostPopularRecyclerViewAdapter.notifyDataSetChanged();
                    getView();
                    //loader.setVisibility(View.GONE);
                }
                //loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<DishesResponse> call, Throwable t) {
                Utils.showSnackbar(getActivity(), R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

    private void getNearestRestaurant() {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.RESTAURANT_URL).getNearestRestaurant(user.lang, 0.0, 0.0, 25.0).enqueue(new Callback<RestaurantResponse>() {
            @Override
            public void onResponse(Call<RestaurantResponse> call, Response<RestaurantResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    restautantData.addAll(response.body().data);
                    homeResturantRecyclerViewAdapter.notifyDataSetChanged();
                }
                //loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<RestaurantResponse> call, Throwable t) {
                Utils.showSnackbar(getActivity(), R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

/*
    private void intiateDialog() {
        Dialog dialog = new Dialog(getActivity(), R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog__home);
        ImageView imageView = dialog.findViewById(R.id.image_city);
        ImageView imageView2 = dialog.findViewById(R.id.image_neborhood);

        dialog.show();
        imageView.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), SeeMoreActivity.class).putExtra("from","search").putExtra("text",searchEt.getText().toString()));
            dialog.dismiss();
        });
        imageView2.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), SeeMoreActivity.class).putExtra("from","search").putExtra("text",searchEt.getText().toString()));
            dialog.dismiss();
        });
        dialog.setCanceledOnTouchOutside(true);


        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
    }
*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.cart_image, R.id.image_filter, R.id.see_more_most_tv, R.id.see_more_restaurant_tv, R.id.see_more_dishes_tv})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cart_image:
                Intent intent = new Intent(HomeFragment.this.getActivity(), ItemInCartActivity.class);
                startActivity(intent);
                break;
            case R.id.image_filter:
                //intiateDialog();
                break;
            case R.id.see_more_most_tv:
                startActivity(new Intent(getActivity(), SeeMoreActivity.class).putExtra("from", "most"));
                break;
            case R.id.see_more_restaurant_tv:
                startActivity(new Intent(getActivity(), SeeMoreActivity.class).putExtra("from", "restaurant"));
                break;
            case R.id.see_more_dishes_tv:
                startActivity(new Intent(getActivity(), SeeMoreActivity.class).putExtra("from", "dishes"));
                break;
        }
    }

    @OnClick(R.id.deliver_image)
    public void onViewClicked() {
        startActivity(new Intent(getContext(),OrdersDetailsForUserActivity.class));
    }
}
