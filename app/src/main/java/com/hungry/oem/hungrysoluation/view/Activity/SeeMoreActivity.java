package com.hungry.oem.hungrysoluation.view.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.Model.RestaurantData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.DishRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.adapters.ResturantsRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.DishesResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.RestaurantResponse;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeeMoreActivity extends AppCompatActivity {

    @BindView(R.id.rc_dishes)
    RecyclerView rcDishes;
    private SessionManager sessionManager;
    private User user;
    String from;
    private ResturantsRecyclerViewAdapter resturantsRecyclerViewAdapter;
    private List<RestaurantData> restaurantData = new ArrayList<>();
    private DishRecyclerViewAdapter dishRecyclerViewAdapter;
    private List<DishesData> dishesData = new ArrayList<>();
    private List<DishesData> mostPopular = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_more);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        from = getIntent().getStringExtra("from");

        if (from.equals("restaurant")) {
            resturantsRecyclerViewAdapter = new ResturantsRecyclerViewAdapter(restaurantData, this, "");
            rcDishes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            rcDishes.setAdapter(resturantsRecyclerViewAdapter);
            rcDishes.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                @Override
                public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                    rv.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }

                @Override
                public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                }

                @Override
                public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                }
            });
            getNearestRestaurant();
        } else if (from.equals("dishes")) {
            dishRecyclerViewAdapter = new DishRecyclerViewAdapter(dishesData, this, "");
            rcDishes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            rcDishes.setAdapter(dishRecyclerViewAdapter);
            rcDishes.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                @Override
                public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                    rv.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }

                @Override
                public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                }

                @Override
                public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                }
            });
            getDishesRetrofit();
        } else if (from.equals("search")) {
            dishRecyclerViewAdapter = new DishRecyclerViewAdapter(dishesData, this, "");
            rcDishes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            rcDishes.setAdapter(dishRecyclerViewAdapter);
            rcDishes.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                @Override
                public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                    rv.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }

                @Override
                public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                }

                @Override
                public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                }
            });
            getDishesRetrofitBySearch(getIntent().getStringExtra("text"));
        } else {
            dishRecyclerViewAdapter = new DishRecyclerViewAdapter(mostPopular, this, "");
            rcDishes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            rcDishes.setAdapter(dishRecyclerViewAdapter);
            rcDishes.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                @Override
                public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                    rv.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }

                @Override
                public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                }

                @Override
                public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                }
            });
            getMostPopular();
        }


    }

    private void getDishesRetrofit() {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getAllDishes(user.lang).enqueue(new Callback<DishesResponse>() {
            @Override
            public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    dishesData.addAll(response.body().data);
                    dishRecyclerViewAdapter.notifyDataSetChanged();
                }else
                    Utils.showSnackbar(SeeMoreActivity.this,response.body().message);

                //loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<DishesResponse> call, Throwable t) {
                Utils.showSnackbar(SeeMoreActivity.this, R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

    private void getDishesRetrofitBySearch(String searchTxt) {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getDishesBySearchHome(searchTxt, user.lon, user.lat).enqueue(new Callback<DishesResponse>() {
            @Override
            public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    dishesData.addAll(response.body().data);
                    dishRecyclerViewAdapter.notifyDataSetChanged();
                }else
                    Utils.showSnackbar(SeeMoreActivity.this,response.body().message);

                //loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<DishesResponse> call, Throwable t) {
                Utils.showSnackbar(SeeMoreActivity.this, R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

    private void getMostPopular() {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getMostPopular(user.lang).enqueue(new Callback<DishesResponse>() {
            @Override
            public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    mostPopular.addAll(response.body().data);
                    dishRecyclerViewAdapter.notifyDataSetChanged();
                    //loader.setVisibility(View.GONE);
                }else
                    Utils.showSnackbar(SeeMoreActivity.this,response.body().message);

                //loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<DishesResponse> call, Throwable t) {
                Utils.showSnackbar(SeeMoreActivity.this, R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

    private void getNearestRestaurant() {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.RESTAURANT_URL).getNearestRestaurant(user.lang, 0.0, 0.0, 25.0).enqueue(new Callback<RestaurantResponse>() {
            @Override
            public void onResponse(Call<RestaurantResponse> call, Response<RestaurantResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    restaurantData.addAll(response.body().data);
                    resturantsRecyclerViewAdapter.notifyDataSetChanged();
                }else
                    Utils.showSnackbar(SeeMoreActivity.this,response.body().message);

                //loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<RestaurantResponse> call, Throwable t) {
                Utils.showSnackbar(SeeMoreActivity.this, R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

    @OnClick({R.id.cart_image, R.id.deliver_image, R.id.filter_image, R.id.back_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cart_image:
                break;
            case R.id.deliver_image:
                break;
            case R.id.filter_image:
                break;
            case R.id.back_image:
                this.finish();
                break;
        }
    }
}
