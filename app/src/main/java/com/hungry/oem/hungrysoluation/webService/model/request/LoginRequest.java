package com.hungry.oem.hungrysoluation.webService.model.request;

/**
 * Created by oem on 9/18/18.
 */

public class LoginRequest {
    private String nameORemail,password;

    public LoginRequest(String nameORemail, String password) {
        this.nameORemail = nameORemail;
        this.password = password;
    }
}
