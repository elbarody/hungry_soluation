package com.hungry.oem.hungrysoluation.view.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.RateRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddReviewActivity extends AppCompatActivity {


    @BindView(R.id.btn_send)
    Button btn_send_review;
    @BindView(R.id.cart_image)
    ImageView card_image;
    @BindView(R.id.back_image)
    ImageView back_image;
    Unbinder unbinder;
    @BindView(R.id.editText)
    EditText editText;


    User user;
    SessionManager sessionManager;
    @BindView(R.id.rating)
    MaterialRatingBar rating;
    @BindView(R.id.loader)
    MKLoader loader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_rate);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();


        unbinder = ButterKnife.bind(this);

        btn_send_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRate();
            }
        });
        Fonty.setFonts(this);

    }

    public void setRate() {
        RetrofitWebService.getService(D.BASE_URL).createRate(new RateRequest(user.id, getIntent().getStringExtra("restaurant_id"), Float.toString(rating.getRating()), editText.getText().toString()),user.lang).enqueue(new Callback<CodeResponse>() {
            @Override
            public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (response.body().errorCode.equals("0")) {
                    intiateDialog();
                    loader.setVisibility(View.GONE);

                    //Utils.showSnackbar(CountactUsActivity.this,R.string.submit);
                } else {
                    Utils.showSnackbar(AddReviewActivity.this, response.body().message);
                    loader.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<CodeResponse> call, Throwable t) {
                Utils.showSnackbar(AddReviewActivity.this, R.string.no_internet_connection);
                loader.setVisibility(View.GONE);
            }
        });

        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddReviewActivity.this.finish();
            }
        });
    }

    private void intiateDialog() {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(AddReviewActivity.this, ShowReviewsActivity.class).
                    putExtra("restaurant_id", getIntent().getStringExtra("restaurant_id"));
            startActivity(intent);
            dialog.dismiss();

        }, 2000);

    }

    @OnClick(R.id.back_image)
    public void onViewClicked() {
        this.finish();
    }
}
