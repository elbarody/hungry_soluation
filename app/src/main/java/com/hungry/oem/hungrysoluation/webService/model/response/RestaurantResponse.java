package com.hungry.oem.hungrysoluation.webService.model.response;

import com.hungry.oem.hungrysoluation.Model.RestaurantData;

import java.util.List;

/**
 * Created by oem on 10/6/18.
 */

public class RestaurantResponse extends CodeResponse {
    public List<RestaurantData> data;
}
