package com.hungry.oem.hungrysoluation.webService.model.request;

public class items {

    private String dishId,price ,qty ,restaurantId;

    public items (String dishId,String price ,String qty,String restaurantId){
        this.dishId = dishId ;
        this.price = price ;
        this.qty = qty ;
        this.restaurantId=restaurantId;
    }

}
