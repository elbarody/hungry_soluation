package com.hungry.oem.hungrysoluation.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hungry.oem.hungrysoluation.Model.RatesData;
import com.hungry.oem.hungrysoluation.R;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.models.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class ReviewsShowRecyclerViewAdapter extends RecyclerView.Adapter<ReviewsShowRecyclerViewAdapter.BH> {

    private static final int FOOTER_VIEW = 1;
    User mUser;

    private List<RatesData> data;
    private Context mContext;
    private Dialog dialog;
    private String from;

    public ReviewsShowRecyclerViewAdapter(List<RatesData> latestOffers, Context context, String from) {
        this.data = latestOffers;
        mContext = context;
        this.from = from;
    }

    @Override
    public BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.reviews_show_rec_row, parent, false);
        BH bh = new BH(v);
        Fonty.setFonts((ViewGroup) v);
        return bh;

    }

    @Override
    public void onBindViewHolder(BH holder, int position) {
        holder.userName.setText(data.get(position).name);
        holder.ratingBar.setRating(Float.parseFloat(data.get(position).value));
        holder.date.setText(data.get(position).datetime);
        holder.reviews.setText(data.get(position).comment);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class BH extends RecyclerView.ViewHolder {
        @BindView(R.id.user_name)
        TextView userName;
        @BindView(R.id.rating_bar)
        MaterialRatingBar ratingBar;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.reviews)
        TextView reviews;

        BH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
