package com.hungry.oem.hungrysoluation.webService.model.request;

public class addressInfo {

    private String customerId, fname, lname, companyname, country, address, city, postno, mobile, email;

    public addressInfo(String customerId, String fname, String lname, String companyname,
                       String country, String address, String city
            , String postno, String mobile, String email) {
        this.customerId = customerId;
        this.fname = fname;
        this.lname = lname;
        this.companyname = companyname;
        this.country = country;
        this.address = address;
        this.city = city;
        this.postno = postno;
        this.mobile = mobile;
        this.email = email;


    }
}
