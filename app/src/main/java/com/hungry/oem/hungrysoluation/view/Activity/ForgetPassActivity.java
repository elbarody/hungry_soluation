package com.hungry.oem.hungrysoluation.view.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.ForgetPassResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPassActivity extends AppCompatActivity {

    @BindView(R.id.ed_email)
    EditText ed_email;
    @BindView(R.id.btn_send)
    Button btn_send;
    @BindView(R.id.loader)
    MKLoader loader;
    private SessionManager sessionManager;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();


        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user.type.equals("user")) {
                    SendPssword(loader, "customer/");
                } else if (user.type.equals("driver")) {
                    SendPssword(loader, "driver/");
                }else
                    SendPssword(loader,"restaurant/");

            }
        });
        Fonty.setFonts(this);

    }

    private void SendPssword(MKLoader loader, String url) {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.BASE_URL + url).SendForgetPass(ed_email.getText().toString(), user.lang).enqueue(new Callback<ForgetPassResponse>() {
            @Override
            public void onResponse(Call<ForgetPassResponse> call, Response<ForgetPassResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (response.body().errorCode.equals("0")) {
                    ForgetPassActivity.this.finish();
                    loader.setVisibility(View.GONE);

                } else {
                    Utils.showSnackbar(ForgetPassActivity.this, response.body().message);
                    loader.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ForgetPassResponse> call, Throwable t) {
                Utils.showSnackbar(ForgetPassActivity.this, R.string.no_internet_connection);
                loader.setVisibility(View.GONE);
            }
        });
    }

}
