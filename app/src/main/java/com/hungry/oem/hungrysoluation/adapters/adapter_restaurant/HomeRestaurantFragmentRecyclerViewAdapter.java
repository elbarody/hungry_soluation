package com.hungry.oem.hungrysoluation.adapters.adapter_restaurant;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hungry.oem.hungrysoluation.Model.SalesData;
import com.hungry.oem.hungrysoluation.R;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.models.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

/**
 * Created by eng on 12/02/18.
 */

public class HomeRestaurantFragmentRecyclerViewAdapter extends RecyclerView.Adapter<HomeRestaurantFragmentRecyclerViewAdapter.BH> {

    private static final int FOOTER_VIEW = 1;
    User mUser;


    private List<SalesData> data;
    private Context mContext;
    private Dialog dialog;
    private String from;

    public HomeRestaurantFragmentRecyclerViewAdapter(List<SalesData> latestOffers, Context context, String from) {
        this.data = latestOffers;
        mContext = context;
        this.from = from;
    }

    @Override
    public BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.rv_item_home_restaurant_fragment, parent, false);
        BH bh = new BH(v);
        Fonty.setFonts((ViewGroup) v);
        return bh;

    }

    @Override
    public void onBindViewHolder(BH holder, int position) {
        holder.textDate.setText(data.get(position).date.split(" ")[0]+"\n"+data.get(position).date.split(" ")[1]);
        holder.recievedOrderTv.setText(mContext.getString(R.string.recieved_orders) + data.get(position).totalReceived);
        holder.unrecievedOrderTv.setText(mContext.getString(R.string.unrecieved_orders) + data.get(position).totalNotReceived);
        holder.priceTv.setText(data.get(position).totalPrice + " SAR");
        holder.totalPriceTv.setText(mContext.getString(R.string.total)+data.get(position).totalPrice + "SAR");
        holder.ratingBar.setRating(Float.parseFloat(data.get(position).evaluationAvg));

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class BH extends RecyclerView.ViewHolder {
        @BindView(R.id.text_date)
        TextView textDate;
        @BindView(R.id.recieved_order_tv)
        TextView recievedOrderTv;
        @BindView(R.id.unrecieved_order_tv)
        TextView unrecievedOrderTv;
        @BindView(R.id.price_tv)
        TextView priceTv;
        @BindView(R.id.rating_bar)
        MaterialRatingBar ratingBar;
        @BindView(R.id.total_price_tv)
        TextView totalPriceTv;

        BH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
