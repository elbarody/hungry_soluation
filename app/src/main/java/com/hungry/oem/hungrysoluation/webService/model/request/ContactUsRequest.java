package com.hungry.oem.hungrysoluation.webService.model.request;

/**
 * Created by oem on 10/18/18.
 */

public class ContactUsRequest {
    private String name, email, subject, message;

    public ContactUsRequest(String name, String email, String subject, String message) {
        this.name = name;
        this.email = email;
        this.subject = subject;
        this.message = message;
    }
}
