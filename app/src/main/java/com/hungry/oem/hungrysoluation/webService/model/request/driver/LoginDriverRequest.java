package com.hungry.oem.hungrysoluation.webService.model.request.driver;

public class LoginDriverRequest {
    private String nameORemail,password;

    public LoginDriverRequest(String nameORemail, String password) {
        this.nameORemail = nameORemail;
        this.password = password;
    }
}
