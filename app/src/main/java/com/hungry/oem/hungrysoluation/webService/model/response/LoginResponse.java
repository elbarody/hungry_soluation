package com.hungry.oem.hungrysoluation.webService.model.response;

/**
 * Created by oem on 9/18/18.
 */

public class LoginResponse extends CodeResponse {
    public UserData data;

    public class UserData {
        public String id,name,mobile,email,address,districtId,userName;
    }
}
