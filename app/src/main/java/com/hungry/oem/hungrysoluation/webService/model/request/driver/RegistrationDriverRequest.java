package com.hungry.oem.hungrysoluation.webService.model.request.driver;

/**
 * Created by oem on 9/22/18.
 */

public class RegistrationDriverRequest {
    private String name,vehicleLicense,vehicleTypeId,vehicleModelId,driverLicense,mobile,email,address,districtId,activationCode,userName,password;

    public RegistrationDriverRequest(String name, String vehicleLicense, String vehicleTypeId, String vehicleModelId, String driverLicense, String mobile, String email, String address, String districtId, String activationCode, String userName, String password) {
        this.name = name;
        this.vehicleLicense = vehicleLicense;
        this.vehicleTypeId = vehicleTypeId;
        this.vehicleModelId = vehicleModelId;
        this.driverLicense = driverLicense;
        this.mobile = mobile;
        this.email = email;
        this.address = address;
        this.districtId = districtId;
        this.activationCode = activationCode;
        this.userName = userName;
        this.password = password;
    }
}
