package com.hungry.oem.hungrysoluation.view.Activity;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.SendActivationCodeRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionDriverManager;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.Driver;
import com.xwady.core.models.User;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Code_ForgetPass_Activity extends AppCompatActivity {

    @BindView(R.id.ed_email)
    EditText ed_email;
    @BindView(R.id.btn_send)
    Button btn_send;
    Unbinder unbinder;
    User user;
    Driver driver;
    SessionManager sessionManager;
    SessionDriverManager sessionDriverManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code__forget_pass_);
        ButterKnife.bind(this);
        sessionDriverManager = new SessionDriverManager(this);
        sessionManager = new SessionManager(this);

        user = sessionManager.getUserDetails();
        driver = sessionDriverManager.getDriverDetails();
        Fonty.setFonts(this);

    }

    @OnClick(R.id.btn_send)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_send:
                if (user.type.equals("user")) {
                    sendActivationCode("customer/");
                } else if (user.type.equals("driver")) {

                }
                break;
        }
    }

    private void sendActivationCode(String type) {
        RetrofitWebService.getService(D.BASE_URL + type).sendActivitionCode(new SendActivationCodeRequest( getIntent().getStringExtra("phone").toString(), ed_email.getText().toString()), user.lang).enqueue(new Callback<CodeResponse>() {
            @Override
            public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (response.body().errorCode.equals("0")) {
                    intiateDailog(response.body().message);
                } else
                    intiateDailogCodeError(response.body().message);
            }

            @Override
            public void onFailure(Call<CodeResponse> call, Throwable t) {

            }
        });
    }

    private void intiateDailog(String message) {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);

        ImageView imageView = dialog.findViewById(R.id.image_checked);
        TextView textView = dialog.findViewById(R.id.dialog_tv);
        textView.setText(message);
        Glide.with(this).load("").error(R.drawable.ic_lock_active).into(imageView);
        dialog.show();
        dialog.setOnDismissListener(dialog1 ->
                Code_ForgetPass_Activity.this.finish()
        );

        dialog.setCanceledOnTouchOutside(true);

        new Handler().postDelayed(() -> {
            dialog.dismiss();
            Code_ForgetPass_Activity.this.finish();

        }, 3000);
    }

    private void intiateDailogCodeError(String message) {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);

        ImageView imageView = dialog.findViewById(R.id.image_checked);
        TextView textView = dialog.findViewById(R.id.dialog_tv);
        textView.setText(message);
        Glide.with(this).load("").error(R.drawable.ic_danger).into(imageView);
        dialog.show();

        dialog.setCanceledOnTouchOutside(true);

        new Handler().postDelayed(() -> {
            dialog.dismiss();
        }, 3000);
    }



/*
    private void SendDriverCode(MKLoader loader) {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.BASE_URL).SendForgetPass(ed_email.getText().toString()).enqueue(new Callback<ForgetPassResponse>() {
            @Override
            public void onResponse(Call<ForgetPassResponse> call, Response<ForgetPassResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (response.body().errorCode.equals("0")) {
                    if (response.body().message.equals("password was sent to your email.")) {
                        Intent intent = new Intent(ForgetPassActivity.this, Code_ForgetPass_Activity.class);
                        startActivity(intent);
                    }
                } else
                    Utils.showSnackbar(ForgetPassActivity.this, response.body().message);
                loader.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ForgetPassResponse> call, Throwable t) {
                Utils.showSnackbar(ForgetPassActivity.this, R.string.no_internet_connection);
                loader.setVisibility(View.GONE);
            }
        });
    }
*/

}
