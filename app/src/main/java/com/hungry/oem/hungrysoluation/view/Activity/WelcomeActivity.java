package com.hungry.oem.hungrysoluation.view.Activity;

import android.content.Intent;
import android.os.Bundle;

import com.codemybrainsout.onboarder.AhoyOnboarderActivity;
import com.codemybrainsout.onboarder.AhoyOnboarderCard;
import com.hungry.oem.hungrysoluation.Model.WelcomeData;
import com.hungry.oem.hungrysoluation.R;
import com.marcinorlowski.fonty.Fonty;

import java.util.ArrayList;
import java.util.List;

public class WelcomeActivity extends AhoyOnboarderActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WelcomeData data= (WelcomeData) getIntent().getSerializableExtra("data");
        AhoyOnboarderCard ahoyOnboarderCard1 = new AhoyOnboarderCard(data.title1, data.desc1, R.drawable.first_start);
        AhoyOnboarderCard ahoyOnboarderCard2 = new AhoyOnboarderCard(data.title2, data.desc2, R.drawable.secound_start);
        AhoyOnboarderCard ahoyOnboarderCard3 = new AhoyOnboarderCard(data.title3, data.desc13, R.drawable.third_start);

      /*  ahoyOnboarderCard1.setBackgroundDrawable(R.drawable.enter);
        ahoyOnboarderCard2.setBackgroundDrawable(R.drawable.enter);
        ahoyOnboarderCard3.setBackgroundDrawable(R.drawable.enter);*/

        List<AhoyOnboarderCard> pages = new ArrayList<>();

        pages.add(ahoyOnboarderCard1);
        pages.add(ahoyOnboarderCard2);
        pages.add(ahoyOnboarderCard3);

        for (AhoyOnboarderCard page : pages) {
            page.setTitleColor(R.color.white);
            page.setDescriptionColor(R.color.white);
        }

        setFinishButtonTitle(R.string.start);
        showNavigationControls(true);
        setImageBackground(R.drawable.enter);

        setInactiveIndicatorColor(R.color.grey_600);
        setActiveIndicatorColor(R.color.white);

        setOnboardPages(pages);
        Fonty.setFonts(this);
    }

    @Override
    public void onFinishButtonPressed() {
        startActivity(new Intent(this,ChooseActivity.class));
        this.finish();
    }
    
}
