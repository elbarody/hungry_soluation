package com.hungry.oem.hungrysoluation.view.Activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.hungry.oem.hungrysoluation.R;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.SessionDriverManager;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.models.Driver;
import com.xwady.core.models.User;

import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseActivity extends AppCompatActivity {


    private User user;
    private Driver driver;
    private SessionManager sessionManager;
    private SessionDriverManager sessionDriverManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);
        ButterKnife.bind(this);
        driver = new Driver();
        sessionDriverManager = new SessionDriverManager(this);
        sessionManager = new SessionManager(this);
        user=sessionManager.getUserDetails();
        if (null == user)
            user = new User();
        Fonty.setFonts(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @OnClick({R.id.button8, R.id.restaurant_btn, R.id.driver_btn, R.id.user_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button8:
                if (null == user || null == user.lang || user.lang.equals("ar")) {
                    user.lang = "en";
                    sessionManager.createLoginSession(user);
                } else {
                    user.lang = "ar";
                    sessionManager.createLoginSession(user);
                }

                Locale loc = new Locale(user.lang);
                Locale.setDefault(loc);
                Configuration config = new Configuration();
                config.locale = loc;
                getBaseContext().getResources().updateConfiguration(config,
                        getBaseContext().getResources().getDisplayMetrics());
                startActivity(new Intent(ChooseActivity.this, ChooseActivity.class));

                ChooseActivity.this.finish();
                break;
            case R.id.restaurant_btn:
                user.type = "restaurant";
                sessionManager.createLoginSession(user);
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.driver_btn:
                user.type = "driver";
                sessionDriverManager.createLoginSession(driver);
                sessionManager.createLoginSession(user);
                startActivity(new Intent(this, LoginActivity.class));
                //Utils.showSnackbar(this, "Work on this");
                break;
            case R.id.user_btn:
                user.type = "user";
                sessionDriverManager.createLoginSession(driver);
                sessionManager.createLoginSession(user);
                startActivity(new Intent(this, LoginActivity.class));
                this.finish();
                break;
        }
    }
}
