package com.hungry.oem.hungrysoluation.webService.model.request;

/**
 * Created by eng on 27/02/18.
 */

public class SendMessageRequest {
    private String token,message;

    public SendMessageRequest(String token, String message) {
        this.token = token;
        this.message = message;
    }
}
