package com.hungry.oem.hungrysoluation.view.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.hungry.oem.hungrysoluation.Model.RatesData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.ReviewsShowRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.ShowReviewsResponse;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowReviewsActivity extends AppCompatActivity {

    @BindView(R.id.reviews)
    RecyclerView rvReviews;
    @BindView(R.id.cart_image)
    ImageView Card_image;
    @BindView(R.id.back_image)
    ImageView back_image;

    Unbinder unbinder;
    private ReviewsShowRecyclerViewAdapter reviewsShowRecyclerViewAdapter;
    private List<RatesData> rateData = new ArrayList<>();
    private User user;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_reviews);

        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        unbinder = ButterKnife.bind(this);

        getDataRateRetrofit();
        reviewsShowRecyclerViewAdapter = new ReviewsShowRecyclerViewAdapter(rateData, ShowReviewsActivity.this, "");
        rvReviews.setLayoutManager(new LinearLayoutManager(ShowReviewsActivity.this, LinearLayoutManager.VERTICAL, false));
        rvReviews.setAdapter(reviewsShowRecyclerViewAdapter);
        rvReviews.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });


        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowReviewsActivity.this.finish();
            }
        });

        Card_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ShowReviewsActivity.this, ItemInCartActivity.class);
                startActivity(intent);
            }
        });
        Fonty.setFonts(this);

    }

    private void getDataRateRetrofit() {
        RetrofitWebService.getService(D.BASE_URL).getAllRates(getIntent().getStringExtra("restaurant_id"), user.lang)
                .enqueue(new Callback<ShowReviewsResponse>() {
                    @Override
                    public void onResponse(Call<ShowReviewsResponse> call, Response<ShowReviewsResponse> response) {
                        if (null == response.body()) {
                            onFailure(call, new Throwable());
                            return;
                        } else if (!response.body().data.isEmpty()) {
                            rateData.addAll(response.body().data);
                            reviewsShowRecyclerViewAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onFailure(Call<ShowReviewsResponse> call, Throwable t) {
                        Utils.showSnackbar(ShowReviewsActivity.this, R.string.no_internet_connection);

                    }
                });
    }
}
