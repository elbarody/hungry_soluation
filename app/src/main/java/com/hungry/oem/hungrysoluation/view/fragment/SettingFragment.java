package com.hungry.oem.hungrysoluation.view.fragment;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.view.Activity.AboutUsActivity;
import com.hungry.oem.hungrysoluation.view.Activity.ChooseActivity;
import com.hungry.oem.hungrysoluation.view.Activity.CountactUsActivity;
import com.hungry.oem.hungrysoluation.view.Activity.DriverActivity.DriverEditProfileActivity;
import com.hungry.oem.hungrysoluation.view.Activity.EditProfileActivity;
import com.hungry.oem.hungrysoluation.view.Activity.MainActivity;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.SessionDriverManager;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.models.Driver;
import com.xwady.core.models.User;

import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SettingFragment extends Fragment {


    Unbinder unbinder;
    private SessionManager sessionManager;
    private User user;

    private SessionDriverManager sessionDriverManager;
    private Driver driver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        Button btn_contact_us = view.findViewById(R.id.contact_us);
        Button btn_About_us = view.findViewById(R.id.about_us);
        Button btn_Edit_profile = view.findViewById(R.id.edit_profile);
        Button btn_share = view.findViewById(R.id.share);
        Button btn_logout = view.findViewById(R.id.logout);

        sessionManager = new SessionManager(getActivity());
        user = sessionManager.getUserDetails();

        sessionDriverManager = new SessionDriverManager(getActivity());
        driver = sessionDriverManager.getDriverDetails();

        btn_logout.setOnClickListener(v -> {
            sessionManager.logoutUser(ChooseActivity.class);
            sessionDriverManager.logoutDriver(ChooseActivity.class);
            getActivity().finish();
        });

        btn_contact_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), CountactUsActivity.class));
            }
        });
        btn_About_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(view.getContext(), AboutUsActivity.class));
            }
        });

        if (null==user.type||!user.type.equals("user")) {
            btn_Edit_profile.setVisibility(View.GONE);
        }
        btn_Edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SessionManager sessionManager = new SessionManager(SettingFragment.this.getContext());
                User user = sessionManager.getUserDetails();
                if (user.type.equals("user")) {
                    startActivity(new Intent(view.getContext(), EditProfileActivity.class));

                } else if (user.type.equals("driver")) {
                    startActivity(new Intent(view.getContext(), DriverEditProfileActivity.class));

                }
            }
        });
        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "My application name");
                    String sAux = "\nLet me recommend you this application\n\n";
                    sAux = sAux + "https://play.google.com/store/apps/details?id=the.package.id \n\n";
                    i.putExtra(Intent.EXTRA_TEXT, sAux);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch (Exception e) {
                    //e.toString();
                }
            }
        });

        Fonty.setFonts((ViewGroup) view);

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.button8)
    public void onViewClicked() {
        if (null == user || null == user.lang || user.lang.equals("ar")) {
            user.lang = "en";
            sessionManager.createLoginSession(user);
        } else {
            user.lang = "ar";
            sessionManager.createLoginSession(user);
        }

        Locale loc = new Locale(user.lang);
        Locale.setDefault(loc);
        Configuration config = new Configuration();
        config.locale = loc;
        getActivity().getResources().updateConfiguration(config,
                getActivity().getResources().getDisplayMetrics());
        getActivity().finish();
        startActivity(getActivity().getIntent());
        //startActivity(new Intent(getActivity(), MainActivity.class));
    }
}
