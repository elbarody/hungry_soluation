package com.hungry.oem.hungrysoluation.webService.model.request;

import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;

import java.util.ArrayList;

public class OrderRequest {

    private String customerId ,datetime,subTotal,deliveyFee ;
    private addressInfo addressInfo ;
    private ArrayList<items> items = new ArrayList<items>();

    public OrderRequest(String customerId, String datetime, String subTotal,
                        String deliveyFee, addressInfo addressInfo, ArrayList<items> items ){
        this.customerId =customerId;
        this.datetime = datetime;
        this.subTotal = subTotal;
        this.deliveyFee = deliveyFee;
        this.addressInfo = addressInfo;
        this.items = items;
    }


}
