package com.hungry.oem.hungrysoluation.webService.model.request;

public class SetDriverRequest {
    private String orderId, restaurantId,driverId;

    public SetDriverRequest(String orderId, String restaurantId, String driverId) {
        this.orderId = orderId;
        this.restaurantId = restaurantId;
        this.driverId = driverId;
    }
}
