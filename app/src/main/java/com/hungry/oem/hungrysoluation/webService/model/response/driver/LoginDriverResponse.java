package com.hungry.oem.hungrysoluation.webService.model.response.driver;

import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;

public class LoginDriverResponse extends CodeResponse {
    public DriverData data;

    public class DriverData {
        public String id , name ,vehicleLicense,vehicleType,vehicleModel,driverLicense,
                mobile,email,address,district,activationCode,userName,password;

    }
}
