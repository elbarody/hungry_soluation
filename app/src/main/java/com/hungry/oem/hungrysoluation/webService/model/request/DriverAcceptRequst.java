package com.hungry.oem.hungrysoluation.webService.model.request;

public class DriverAcceptRequst {
    private String orderId, driverId;

    public DriverAcceptRequst(String orderId, String driverId) {
        this.orderId = orderId;
        this.driverId = driverId;
    }
}
