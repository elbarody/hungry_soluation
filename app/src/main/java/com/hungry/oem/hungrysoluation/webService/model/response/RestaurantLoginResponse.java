package com.hungry.oem.hungrysoluation.webService.model.response;

/**
 * Created by oem on 11/3/18.
 */

public class RestaurantLoginResponse extends CodeResponse{
    public RestaurantDataResponse data;


    public class RestaurantDataResponse {
        public String id,responsible, name, license,
                email, mobile,userName, password,
                address, logoImgeURL, Longitude, Latitude,deliveryFee;
    }
}
