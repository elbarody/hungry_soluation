package com.hungry.oem.hungrysoluation.view.fragment.fragmentRestaurant;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.hungry.oem.hungrysoluation.Model.BestSaleData;
import com.hungry.oem.hungrysoluation.Model.SalesData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.adapter_restaurant.HomeRestaurantFragmentRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.adapters.adapter_restaurant.SaleRestaurantFragmentRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.BestSalesResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.driver.SalesResponse;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by oem on 10/28/18.
 */

public class BestBuysFragment extends Fragment {
    @BindView(R.id.rec_sales)
    RecyclerView recSales;
    @BindView(R.id.day)
    Button tv_day;
    @BindView(R.id.month)
    Button tv_month;
    @BindView(R.id.week)
    Button tv_week;
    private Unbinder unbinder;
    private SaleRestaurantFragmentRecyclerViewAdapter saleAdapter;
    private SessionManager sessionManger;
    private User user;
    private ArrayList<BestSaleData> salesData=new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_best_buys_restaurant, container, false);
        unbinder = ButterKnife.bind(this, view);

        sessionManger=new SessionManager(getContext());
        user=sessionManger.getUserDetails();
        getSalesRetrofit("day", user.id);
        saleAdapter = new SaleRestaurantFragmentRecyclerViewAdapter(salesData,getActivity(), "");
        recSales.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recSales.setAdapter(saleAdapter);
        recSales.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        //getDishesRetrofit();

        Fonty.setFonts((ViewGroup) view);

        return view;
    }

    @SuppressLint("ResourceAsColor")
    @OnClick({R.id.day, R.id.month, R.id.week})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.day:
                tv_day.setBackgroundResource(R.drawable.border_right);
                tv_month.setBackgroundResource(R.drawable.border);
                tv_week.setBackgroundResource(R.drawable.border);
                getSalesRetrofit("day", user.id);
                break;
            case R.id.week:
                tv_day.setBackgroundResource(R.drawable.border);
                tv_month.setBackgroundResource(R.drawable.border);
                tv_week.setBackgroundResource(R.drawable.border_right);
                getSalesRetrofit("week", user.id);
                break;
            case R.id.month:
                tv_day.setBackgroundResource(R.drawable.border);
                tv_month.setBackgroundResource(R.drawable.border_right);
                tv_week.setBackgroundResource(R.drawable.border);
                getSalesRetrofit("month", user.id);
                break;
        }
    }

    private void getSalesRetrofit(String from, String id) {
        //mkLoader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.ORDER_URL).getBestSalesForRestaurant(from, id,user.lang).enqueue(new Callback<BestSalesResponse>() {
            @Override
            public void onResponse(Call<BestSalesResponse> call, Response<BestSalesResponse> response) {
                salesData.clear();
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    //mkLoader.setVisibility(View.GONE);
                    return;
                } else if (response.body().errorCode.equals("3")) {
                    Toast.makeText(getContext(), response.body().message, Toast.LENGTH_LONG).show();
                    saleAdapter.notifyDataSetChanged();

                    //mkLoader.setVisibility(View.GONE);
                } else if (!response.body().data.isEmpty()) {
                    salesData.clear();
                    salesData.addAll(response.body().data);
                    saleAdapter.notifyDataSetChanged();
                    //mkLoader.setVisibility(View.GONE);
                }
                //mkLoader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<BestSalesResponse> call, Throwable t) {
                Utils.showSnackbar(Objects.requireNonNull(BestBuysFragment.this.getActivity()), R.string.no_internet_connection);
                //mkLoader.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
