package com.hungry.oem.hungrysoluation.webService.model.response.driver;

import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.Model.SalesData;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;

import java.util.List;

public class SalesResponse extends CodeResponse{

    public List<SalesData> data;
}
