package com.hungry.oem.hungrysoluation.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.SaveLocationRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionDriverManager;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.models.Driver;
import com.xwady.core.models.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by barody on 5/15/18.
 */

public class BackgroundService extends Service {
    private boolean isRunning;
    private Context context;
    private Thread backgroundThread;
    private Driver user;
    private Runnable myTask = new Runnable() {
        public void run() {
            if (null == user) {
                return;
            }
            RetrofitWebService.getService(D.DRIVER_URL).saveMyLocation(new SaveLocationRequest(user.id, Double.toString(user.lon), Double.toString(user.lat))).enqueue(new Callback<CodeResponse>() {
                @Override
                public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                    if (null == response.body()) {
                        onFailure(call, new Throwable());
                        return;
                    } else if (response.body().errorCode.equals("0")) {
                        Log.d("onResponse: ", user.lon + " + " + user.lat);
                    }
                }

                @Override
                public void onFailure(Call<CodeResponse> call, Throwable t) {

                }
            });
            System.out.println("elbarody 15 sec");
            // Do something here
            stopSelf();
            isRunning = false;
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        this.context = this;
        SessionDriverManager sessionManager = new SessionDriverManager(context);
        user = sessionManager.getDriverDetails();

        this.isRunning = false;
        this.backgroundThread = new Thread(myTask);
    }

    @Override
    public void onDestroy() {
        this.isRunning = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!this.isRunning) {
            if (backgroundThread.getState() == Thread.State.NEW) {
                this.isRunning = true;
                this.backgroundThread.start();
            }
        }
        return START_STICKY;
    }
}
