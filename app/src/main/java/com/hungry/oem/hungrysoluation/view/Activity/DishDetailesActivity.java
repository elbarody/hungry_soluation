package com.hungry.oem.hungrysoluation.view.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.DishesToOrderRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.DishesResponse;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DishDetailesActivity extends AppCompatActivity {

    @BindView(R.id.rec_dishs)
    RecyclerView rv_dishes;
    @BindView(R.id.show_review)
    ImageView show_review;
    @BindView(R.id.add_review)
    ImageView add_review;
    @BindView(R.id.cart_image)
    ImageView cart;
    @BindView(R.id.btn_plus)
    ImageView btn_pluse;
    @BindView(R.id.btn_minus)
    ImageView btn_minus;
    @BindView(R.id.num)
    TextView num;
    @BindView(R.id.dish_name)
    TextView dish_name;
    @BindView(R.id.dish_price)
    TextView dish_price;
    @BindView(R.id.dish_discription)
    TextView dish_discription;
    @BindView(R.id.dish_image)
    ImageView dish_image;
    @BindView(R.id.add_to_card)
    Button btn_card;

    Unbinder unbinder;
    DishesData dishData;
    private List<DishesData> dishesData = new ArrayList<>();
    private DishesToOrderRecyclerViewAdapter dishesToOrderRecyclerViewAdapter;
    private SessionManager sessionManager;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish_detailes);

        unbinder = ButterKnife.bind(this);

        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        dishData = (DishesData) getIntent().getSerializableExtra("data");

        dish_name.setText(dishData.name);
        dish_price.setText(dishData.price + " " + getString(R.string.price));
        dish_discription.setText(dishData.descText);
        Glide.with(DishDetailesActivity.this).load(D.IMAGE_URL + dishData.imageUrl).error(R.drawable.background3).into(dish_image);

        num.setText("1");
        btn_pluse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int numm = Integer.parseInt(num.getText().toString());
                numm++;
                num.setText("" + numm);
            }
        });

        btn_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int numm = Integer.parseInt(num.getText().toString());
                if (numm > 1) {
                    numm--;
                }
                num.setText("" + numm);
            }
        });


        dishesToOrderRecyclerViewAdapter = new DishesToOrderRecyclerViewAdapter(dishesData, this, "");
        rv_dishes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv_dishes.setAdapter(dishesToOrderRecyclerViewAdapter);
        rv_dishes.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
        getDishesRetrofit();

        show_review.setOnClickListener(view -> {
            Intent intent = new Intent(DishDetailesActivity.this, ShowReviewsActivity.class).putExtra("restaurant_id", dishData.restaurantId);
            startActivity(intent);
        });

        add_review.setOnClickListener(view -> {
            Intent intent = new Intent(DishDetailesActivity.this, AddReviewActivity.class).putExtra("restaurant_id", dishData.restaurantId);
            startActivity(intent);
        });


        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DishDetailesActivity.this, ItemInCartActivity.class);
                startActivity(intent);
            }
        });


        btn_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DishesData dishesData = new DishesData();
                dishesData.count = Integer.parseInt(num.getText().toString());
                dishesData.descText = dishData.descText;
                dishesData.discount = dishData.discount;
                dishesData.id = dishData.id;
                dishesData.restaurantId = dishData.restaurantId;
                dishesData.name = dishData.name;
                int total = Integer.parseInt(dishData.price);
                dishesData.price = String.valueOf(total * dishesData.count);
                dishesData.imageUrl = dishData.imageUrl;
                for (int i = 0; i < MainActivity.card_list.size(); i++) {
                    if (!MainActivity.card_list.get(i).restaurantId.equals(dishesData.restaurantId)) {
                        Toast.makeText(DishDetailesActivity.this, R.string.you_cant_order_from_diffrent_places, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                MainActivity.card_list.add(dishesData);
                intiateDialog();
                //Toast.makeText(DishDetailesActivity.this, "dish Added", Toast.LENGTH_SHORT).show();
            }
        });
        Fonty.setFonts(this);


    }

    private void intiateDialog() {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);

        dialog.setContentView(R.layout.custom_dialog_done);
        TextView dialogTv = dialog.findViewById(R.id.dialog_tv);
        dialogTv.setText(R.string.dish_added);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        new Handler().postDelayed(() -> {

            dialog.dismiss();

        }, 2000);

    }

    private void getDishesRetrofit() {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getAllDishes(user.lang).enqueue(new Callback<DishesResponse>() {
            @Override
            public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    dishesData.addAll(response.body().data);
                    dishesToOrderRecyclerViewAdapter.notifyDataSetChanged();
                }
                //loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<DishesResponse> call, Throwable t) {
                Utils.showSnackbar(DishDetailesActivity.this, R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

}
