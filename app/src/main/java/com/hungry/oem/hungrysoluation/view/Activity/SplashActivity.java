package com.hungry.oem.hungrysoluation.view.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.view.Activity.DriverActivity.MainActivityDriver;
import com.hungry.oem.hungrysoluation.view.Activity.restaurantActivity.MainRestaurantActivity;
import com.hungry.oem.hungrysoluation.webService.RetrofitService;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.WelcomeResponse;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionDriverManager;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.Driver;
import com.xwady.core.models.User;

import io.fabric.sdk.android.Fabric;
import java.io.Serializable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private User user;
    private Driver driver;
    private SessionManager sessionManager;
    private SessionDriverManager sessionDriverManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        sessionManager = new SessionManager(this);
        sessionDriverManager = new SessionDriverManager(this);

        user = sessionManager.getUserDetails();
        driver = sessionDriverManager.getDriverDetails();
        new Handler().postDelayed(() -> {
            if (null == user || null == driver) {
                getWelcome();
            } else if (user.getLoginStatus() == User.AccountStatus.LOGGED_IN_USER) {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                SplashActivity.this.finish();
            }else if (driver.getLoginStatus() == Driver.AccountStatus.LOGGED_IN_DRIVER) {
                startActivity(new Intent(SplashActivity.this, MainActivityDriver.class));
                SplashActivity.this.finish();
            } else if (user.getLoginStatus() == User.AccountStatus.LOGGED_IN_RESTAURANT) {
                startActivity(new Intent(SplashActivity.this, MainRestaurantActivity.class));
                SplashActivity.this.finish();
            }
            else {
                getWelcome();

            }


        }, 1500);

    }

    private void getWelcome(){
        RetrofitWebService.getService(D.BASE_URL).getWelcome().enqueue(new Callback<WelcomeResponse>() {
            @Override
            public void onResponse(Call<WelcomeResponse> call, Response<WelcomeResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (response.body().errorCode.equals("0")) {
                    startActivity(new Intent(SplashActivity.this, WelcomeActivity.class).putExtra("data",response.body().data));
                    SplashActivity.this.finish();
                }

            }

            @Override
            public void onFailure(Call<WelcomeResponse> call, Throwable t) {
                Utils.showSnackbar(SplashActivity.this, R.string.no_internet_connection);

            }
        });
    }
}
