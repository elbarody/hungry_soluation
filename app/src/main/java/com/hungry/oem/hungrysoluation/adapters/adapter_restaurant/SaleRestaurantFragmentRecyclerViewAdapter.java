package com.hungry.oem.hungrysoluation.adapters.adapter_restaurant;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.Model.BestSaleData;
import com.hungry.oem.hungrysoluation.R;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.models.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by eng on 12/02/18.
 */

public class SaleRestaurantFragmentRecyclerViewAdapter extends RecyclerView.Adapter<SaleRestaurantFragmentRecyclerViewAdapter.BH> {

    private static final int FOOTER_VIEW = 1;
    User mUser;


    private List<BestSaleData> data;
    private Context mContext;
    private Dialog dialog;
    private String from;

    public SaleRestaurantFragmentRecyclerViewAdapter(List<BestSaleData> latestOffers, Context context, String from) {
        this.data = latestOffers;
        mContext = context;
        this.from = from;
    }

    @Override
    public BH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.rv_item_sale_restaurant_fragment, parent, false);
        BH bh = new BH(v);
        Fonty.setFonts((ViewGroup) v);
        return bh;

    }

    @Override
    public void onBindViewHolder(BH holder, int position) {
        holder.nameOrderTv.setText(data.get(position).name);
        holder.saleNumberTv.setText(data.get(position).totalCount);
        holder.totalPriceTv.setText(data.get(position).total_price);
        holder.priceTv.setText(data.get(position).price);
        Glide.with(mContext).load(D.IMAGE_URL + data.get(position).imageUrl).into(holder.imageDish);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class BH extends RecyclerView.ViewHolder {

        @BindView(R.id.image_dish)
        ImageView imageDish;
        @BindView(R.id.name_order_tv)
        TextView nameOrderTv;
        @BindView(R.id.sale_number_tv)
        TextView saleNumberTv;
        @BindView(R.id.price_tv)
        TextView priceTv;
        @BindView(R.id.total_price_tv)
        TextView totalPriceTv;

        BH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
