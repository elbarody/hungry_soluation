package com.hungry.oem.hungrysoluation.view.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.widget.Toast;

import com.hungry.oem.hungrysoluation.Model.RestaurantOrderData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.adapter_restaurant.OrdersRestaurantFragmentRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.RestaurantOrdersResponse;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersDetailsForUserActivity extends AppCompatActivity {

    @BindView(R.id.rec_orders)
    RecyclerView recOrders;
    User user;
    SessionManager sessionManager;
    private OrdersRestaurantFragmentRecyclerViewAdapter adapter;
    private List<RestaurantOrderData> data = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_details_for_user);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();

        adapter = new OrdersRestaurantFragmentRecyclerViewAdapter(data, this, "user");
        recOrders.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recOrders.setAdapter(adapter);
        recOrders.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        getOrdersRetrofit();

        Fonty.setFonts(this);

    }

    private void getOrdersRetrofit() {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.ORDER_URL).getAllOrdersForUser(user.id, user.lang).enqueue(new Callback<RestaurantOrdersResponse>() {
            @Override
            public void onResponse(Call<RestaurantOrdersResponse> call, Response<RestaurantOrdersResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    //loader.setVisibility(View.GONE);
                    return;
                } else if (response.body().errorCode.equals("3")) {
                    data.clear();
                    Toast.makeText(OrdersDetailsForUserActivity.this, response.body().message, Toast.LENGTH_LONG).show();
                    //loader.setVisibility(View.GONE);
                } else if (!response.body().data.isEmpty()) {
                    data.clear();
                    data.addAll(response.body().data);
                    adapter.notifyDataSetChanged();
                    //loader.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<RestaurantOrdersResponse> call, Throwable t) {
                Utils.showSnackbar(OrdersDetailsForUserActivity.this, R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }
}
