package com.hungry.oem.hungrysoluation.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hungry.oem.hungrysoluation.Model.Messages;
import com.hungry.oem.hungrysoluation.R;
import com.marcinorlowski.fonty.Fonty;
import com.xwady.core.models.User;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by eng on 14/02/18.
 */

public class MessagesRecyclerViewAdapter extends RecyclerView.Adapter<MessagesRecyclerViewAdapter.BH> {

    private static int SENDER_TYPE = 1, RECIEVER_TYPE = 2;
    User mUser;
    private List<Messages> messagesList;
    private Context mContext;
    private Dialog dialog;


    public MessagesRecyclerViewAdapter(List<Messages> homeOffers, Context context) {
        this.messagesList = homeOffers;
        mContext = context;
    }

    public void addItem(String item) {
        messagesList.add(0,new Messages(0, item));
    }



    @Override
    public int getItemViewType(int position) {
        if (messagesList.get(position).reply == 1)
            return SENDER_TYPE;

        if (messagesList.get(position).reply == 0)
            return RECIEVER_TYPE;

        return super.getItemViewType(position);
    }

    @Override
    public BH onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = null;
        if (viewType == RECIEVER_TYPE) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_message_client, parent, false);
            Fonty.setFonts((ViewGroup) view);

            return new BH(view);

        }
        view = LayoutInflater.from(mContext).inflate(R.layout.item_message_serve, parent, false);
        Fonty.setFonts((ViewGroup) view);
        return new BH(view);
    }

    @Override
    public void onBindViewHolder(BH holder, final int position) {
        holder.message.setText(messagesList.get(position).message);
    }

    @Override
    public int getItemCount() {
        return messagesList.size();
    }


    static class BH extends RecyclerView.ViewHolder {
        @BindView(R.id.message)
        TextView message;

        BH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
