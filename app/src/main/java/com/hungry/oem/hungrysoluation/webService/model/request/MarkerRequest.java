package com.hungry.oem.hungrysoluation.webService.model.request;

public class MarkerRequest {
    private double Latitude, Longitude;

    public MarkerRequest(double latitude, double longitude) {
        Latitude = latitude;
        Longitude = longitude;
    }
}
