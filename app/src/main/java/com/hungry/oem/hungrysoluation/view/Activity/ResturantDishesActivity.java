package com.hungry.oem.hungrysoluation.view.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.DishRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.adapters.ResturantsRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.DishesResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ResturantDishesActivity extends AppCompatActivity {


    @BindView(R.id.rec_dishs)
    RecyclerView rvDishes;
    @BindView(R.id.card_rate)
    CardView card_rate;
    @BindView(R.id.rest_name)
    TextView rest_name;
    @BindView(R.id.phone_tv)
    TextView rest_phone;
    @BindView(R.id.location_tv)
    TextView rest_location;
    @BindView(R.id.text_average)
    TextView text_average;
    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.rating_bar)
    MaterialRatingBar rating_bar;

    Unbinder unbinder;
    @BindView(R.id.loader)
    MKLoader loader;
    private DishRecyclerViewAdapter dishRecyclerViewAdapter;
    @BindView(R.id.back_image)
    ImageView back_image;

    List<DishesData> dishesData = new ArrayList<>();
    private String filter;
    private SessionManager sessionManager;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resturant_dishes);
        unbinder = ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        rest_name.setText(ResturantsRecyclerViewAdapter.restaurantData.name);
        rest_location.setText(ResturantsRecyclerViewAdapter.restaurantData.address);
        rest_phone.setText(ResturantsRecyclerViewAdapter.restaurantData.mobile);
        text_average.setText(ResturantsRecyclerViewAdapter.restaurantData.average);
        rating_bar.setRating(ResturantsRecyclerViewAdapter.restaurantData.rate);
        Glide.with(ResturantDishesActivity.this).load(D.IMAGE_URL + ResturantsRecyclerViewAdapter.restaurantData.logoImgeURL).error(R.drawable.background2).into(logo);

        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResturantDishesActivity.this.finish();
            }
        });

        dishRecyclerViewAdapter = new DishRecyclerViewAdapter(dishesData, ResturantDishesActivity.this, "");
        rvDishes.setLayoutManager(new LinearLayoutManager(ResturantDishesActivity.this, LinearLayoutManager.VERTICAL, false));
        rvDishes.setAdapter(dishRecyclerViewAdapter);
        rvDishes.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });


        /*card_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ResturantDishesActivity.this, AddReviewActivity.class);
                startActivity(intent);
            }
        });*/

        getResturantDishesRetrofit();
        Fonty.setFonts(this);


    }

    private void getResturantDishesRetrofit() {
//        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getRestaurantDishes(ResturantsRecyclerViewAdapter.restaurantData.id, user.lang).enqueue(new Callback<DishesResponse>() {
            @Override
            public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    return;
                } else if (!response.body().data.isEmpty()) {
                    dishesData.addAll(response.body().data);
                    dishRecyclerViewAdapter.notifyDataSetChanged();
                }
                // loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<DishesResponse> call, Throwable t) {
                Utils.showSnackbar(ResturantDishesActivity.this, R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

    @OnClick({R.id.deliver_image, R.id.filter_image, R.id.cart_image, R.id.card_rate})
    public void onViewClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.card_rate:
                intent = new Intent(this, ShowReviewsActivity.class).
                        putExtra("restaurant_id", ResturantsRecyclerViewAdapter.restaurantData.id);
                startActivity(intent);
                break;
            case R.id.deliver_image:
                intiateDialogSearch();
                break;
            case R.id.filter_image:
                intiateDialog();
                break;
            case R.id.cart_image:
                intent = new Intent(ResturantDishesActivity.this, ItemInCartActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void intiateDialog() {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_dishes);
        ImageView popular = dialog.findViewById(R.id.image_propularity);
        ImageView newness = dialog.findViewById(R.id.image_newness);
        ImageView average = dialog.findViewById(R.id.image_average);
        ImageView lowToHigh = dialog.findViewById(R.id.image_low_to_hight);
        ImageView highToLow = dialog.findViewById(R.id.image_high_to_low);

        dialog.show();
        popular.setOnClickListener(v -> {
            filter = "moreLovely";
            getFilterDishesRetrofit();
            dialog.dismiss();
        });

        newness.setOnClickListener(v -> {
            filter = "recently";
            getFilterDishesRetrofit();
            dialog.dismiss();
        });

        average.setOnClickListener(v -> {
            filter = "avg";
            getFilterDishesRetrofit();
            dialog.dismiss();
        });

        lowToHigh.setOnClickListener(v -> {
            filter = "asc";
            getFilterDishesRetrofit();
            dialog.dismiss();
        });

        highToLow.setOnClickListener(v -> {
            filter = "desc";
            getFilterDishesRetrofit();
            dialog.dismiss();
        });
        dialog.setCanceledOnTouchOutside(true);


        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
    }

    private void intiateDialogSearch() {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_search);
        ImageView search = dialog.findViewById(R.id.image_search);
        EditText editText = dialog.findViewById(R.id.search_et);
        editText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                getSearchDishesRetrofit(editText.getText().toString());
                dialog.dismiss();
                return true;
            }
            return false;
        });
        dialog.show();


        search.setOnClickListener(v -> {
            //filter = "desc";

        });
        dialog.setCanceledOnTouchOutside(true);


        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
    }

    private void getSearchDishesRetrofit(String search) {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getAllDishesSearchForRestaurant(search, ResturantsRecyclerViewAdapter.restaurantData.id, user.lang)
                .enqueue(new Callback<DishesResponse>() {
                    @Override
                    public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                        if (null == response.body()) {
                            onFailure(call, new Throwable());
                            return;
                        } else if (!response.body().data.isEmpty()) {
                            dishesData.clear();
                            dishesData.addAll(response.body().data);
                            dishRecyclerViewAdapter.notifyDataSetChanged();
                        }
                        loader.setVisibility(View.GONE);

                    }

                    @Override
                    public void onFailure(Call<DishesResponse> call, Throwable t) {
                        Utils.showSnackbar(ResturantDishesActivity.this, R.string.no_internet_connection);
                        loader.setVisibility(View.GONE);
                    }
                });
    }

    private void getFilterDishesRetrofit() {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).getAllDishesFilteredForRestaurant(filter, ResturantsRecyclerViewAdapter.restaurantData.id, user.lang)
                .enqueue(new Callback<DishesResponse>() {
                    @Override
                    public void onResponse(Call<DishesResponse> call, Response<DishesResponse> response) {
                        if (null == response.body()) {
                            onFailure(call, new Throwable());
                            return;
                        } else if (!response.body().data.isEmpty()) {
                            dishesData.clear();
                            dishesData.addAll(response.body().data);
                            dishRecyclerViewAdapter.notifyDataSetChanged();
                        }
                        loader.setVisibility(View.GONE);

                    }

                    @Override
                    public void onFailure(Call<DishesResponse> call, Throwable t) {
                        Utils.showSnackbar(ResturantDishesActivity.this, R.string.no_internet_connection);
                        loader.setVisibility(View.GONE);
                    }
                });
    }


    @OnClick(R.id.chat)
    public void onViewClicked() {
        startActivity(new Intent(this, MessagesActivity.class));
    }

}
