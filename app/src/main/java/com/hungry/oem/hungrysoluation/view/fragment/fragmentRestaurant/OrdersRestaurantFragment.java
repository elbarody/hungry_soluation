package com.hungry.oem.hungrysoluation.view.fragment.fragmentRestaurant;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.hungry.oem.hungrysoluation.Model.RestaurantOrderData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.adapter_restaurant.OrdersRestaurantFragmentRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.RestaurantOrdersResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.driver.SalesResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by oem on 10/28/18.
 */

public class OrdersRestaurantFragment extends Fragment {

    @BindView(R.id.rec_orders)
    RecyclerView recHome;

    private Unbinder unbinder;
    private OrdersRestaurantFragmentRecyclerViewAdapter adapter;

    User user;
    SessionManager sessionManager;
    private List<RestaurantOrderData> data=new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.orders_restaurant_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        sessionManager=new SessionManager(getActivity());
        user=sessionManager.getUserDetails();

        adapter = new OrdersRestaurantFragmentRecyclerViewAdapter(data, getActivity(), "");
        recHome.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recHome.setAdapter(adapter);
        recHome.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        getOrdersRetrofit();

        Fonty.setFonts((ViewGroup) view);

        return view;
    }

    private void getOrdersRetrofit() {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.ORDER_URL).getAllOrdersForRestaurant(user.id,user.lang).enqueue(new Callback<RestaurantOrdersResponse>() {
            @Override
            public void onResponse(Call<RestaurantOrdersResponse> call, Response<RestaurantOrdersResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    //loader.setVisibility(View.GONE);
                    return;
                } else if (response.body().errorCode.equals("3")) {
                    data.clear();
                    Toast.makeText(getActivity(), response.body().message, Toast.LENGTH_LONG).show();
                    //loader.setVisibility(View.GONE);
                } else if (!response.body().data.isEmpty()) {
                    data.clear();
                    data.addAll(response.body().data);
                    adapter.notifyDataSetChanged();
                    //loader.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<RestaurantOrdersResponse> call, Throwable t) {
                Utils.showSnackbar(getActivity(), R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
