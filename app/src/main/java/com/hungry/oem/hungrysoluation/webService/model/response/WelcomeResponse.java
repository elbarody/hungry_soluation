package com.hungry.oem.hungrysoluation.webService.model.response;

import com.hungry.oem.hungrysoluation.Model.WelcomeData;

import java.io.Serializable;

public class WelcomeResponse extends CodeResponse  {
    public WelcomeData data;
}
