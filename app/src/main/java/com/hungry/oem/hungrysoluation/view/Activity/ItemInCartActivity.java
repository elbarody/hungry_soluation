package com.hungry.oem.hungrysoluation.view.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.DishInCartRecyclerViewAdapter;
import com.marcinorlowski.fonty.Fonty;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ItemInCartActivity extends AppCompatActivity {


    //@BindView(R.id.item_cart)
    public static RecyclerView rec_item_cart;

    @BindView(R.id.btn_send)
    Button btn_send;
    @BindView(R.id.back_image)
    ImageView back_image;


    Unbinder unbinder;
    public static DishInCartRecyclerViewAdapter dishInCartRecyclerViewAdapter;
    public static TextView sub_toal, shipping, total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_in_cart);
        rec_item_cart = findViewById(R.id.item_cart);
        sub_toal = findViewById(R.id.sub_total);
        total = findViewById(R.id.total);



        int subtotal = 0, tottal = 0;
        for (int i = 0; i < MainActivity.card_list.size(); i++) {
            int price = Integer.parseInt(MainActivity.card_list.get(i).price);
            subtotal += price;
        }

        sub_toal.setText(getString(R.string.subtotal) + "   " + subtotal + "   " + getString(R.string.sar));
        if (subtotal == 0) {
            tottal = 0;
        } else {
            tottal = subtotal + 10;
        }

        total.setText(getString(R.string.total) + "   " + tottal + "  " + getString(R.string.sar));

        unbinder = ButterKnife.bind(this);

        dishInCartRecyclerViewAdapter = new DishInCartRecyclerViewAdapter(MainActivity.card_list, this, "");
        rec_item_cart.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rec_item_cart.setAdapter(dishInCartRecyclerViewAdapter);
        rec_item_cart.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });


        btn_send.setOnClickListener(view -> {
            Intent intent = new Intent(ItemInCartActivity.this, SendOrderActivity.class);
            startActivity(intent);
            ItemInCartActivity.this.finish();
        });
        Fonty.setFonts(this);

    }

    @OnClick(R.id.back_image)
    public void onViewClicked() {
        ItemInCartActivity.this.finish();
    }
}
