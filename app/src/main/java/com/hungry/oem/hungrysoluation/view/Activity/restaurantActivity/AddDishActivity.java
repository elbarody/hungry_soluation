package com.hungry.oem.hungrysoluation.view.Activity.restaurantActivity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.request.restaurant.AddDishRequest;
import com.hungry.oem.hungrysoluation.webService.model.response.CodeResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.User;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDishActivity extends AppCompatActivity {

    @BindView(R.id.loader)
    MKLoader loader;
    @BindView(R.id.image_fill)
    ImageView imageFill;
    @BindView(R.id.dish_category)
    EditText dishCategory;
    @BindView(R.id.num)
    TextView num;
    @BindView(R.id.num2)
    TextView num2;
    @BindView(R.id.dish_description)
    EditText dishDescription;

    User user;
    SessionManager sessionManager;

    private String encodedImage = "";
    private Bitmap bitmap;
    private ByteArrayOutputStream byteArrayOutputStream;
    private static final int READ_STORAGE_CODE = 1001;
    private static final int WRITE_STORAGE_CODE = 1002;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_dish);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails();
        num.setText("1");
        num2.setText("1");
        Fonty.setFonts(this);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (resultCode == -1 && (requestCode == 1001)) {
            Uri selectedImage = imageReturnedIntent.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
            byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream); //bitmap is the bitmap object
            byte[] byteArrayImage = byteArrayOutputStream.toByteArray();
            if (requestCode == 1001) {
                Glide.with(AddDishActivity.this).load(selectedImage).into(imageFill);
                encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
            }

        }
    }

    private void openImagePickerIntent(int PICK_IMAGE) {
        if (isPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, PICK_IMAGE);
        } else {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, READ_STORAGE_CODE);
        }
    }

    private boolean isPermissionGranted(String permission) {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, permission);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }


    //Requesting permission
    private void requestPermission(String permission, int code) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{permission}, code);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == READ_STORAGE_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                openImagePickerIntent(1001);


            } else {

                AddDishActivity.this.finish();
            }
        } else if (requestCode == WRITE_STORAGE_CODE) {


            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {

                AddDishActivity.this.finish();
            }
        }
    }

    @OnClick({R.id.image_add, R.id.btn_minus, R.id.btn_plus, R.id.btn_minus2, R.id.btn_plus2, R.id.add_btn})
    public void onViewClicked(View view) {
        int numm;
        switch (view.getId()) {
            case R.id.image_add:
                openImagePickerIntent(1001);
                break;
            case R.id.btn_minus:
                numm = Integer.parseInt(num.getText().toString());
                if (numm > 1) {
                    numm--;
                }
                num.setText("" + numm);
                break;
            case R.id.btn_plus:
                numm = Integer.parseInt(num.getText().toString());
                numm++;
                num.setText("" + numm);
                break;
            case R.id.btn_minus2:
                numm = Integer.parseInt(num2.getText().toString());
                if (numm > 1) {
                    numm--;
                }
                num2.setText("" + numm);
                break;
            case R.id.btn_plus2:
                numm = Integer.parseInt(num2.getText().toString());
                numm++;
                num2.setText("" + numm);
                break;
            case R.id.add_btn:
                addDishRetrofit();
                break;
        }
    }

    private void addDishRetrofit() {
        loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.DISHES_URL).createDishForRestaurant(new AddDishRequest(user.id, dishCategory.getText().toString(), dishDescription.getText().toString(), num.getText().toString(), num2.getText().toString(), encodedImage),user.lang)
                .enqueue(new Callback<CodeResponse>() {
                    @Override
                    public void onResponse(Call<CodeResponse> call, Response<CodeResponse> response) {
                        if (null == response.body()) {
                            onFailure(call, new Throwable());
                            return;
                        } else if (response.body().errorCode.equals("0")) {
                            intiateDialog();
                            loader.setVisibility(View.GONE);

                        } else {
                            Utils.showSnackbar(AddDishActivity.this, response.body().message);
                            loader.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onFailure(Call<CodeResponse> call, Throwable t) {
                        Utils.showSnackbar(AddDishActivity.this, R.string.no_internet_connection);
                        loader.setVisibility(View.GONE);
                    }
                });
    }

    private void intiateDialog() {
        Dialog dialog = new Dialog(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert);
        dialog.setContentView(R.layout.custom_dialog_done);

        dialog.show();
        dialog.setOnDismissListener(dialog1 ->
                AddDishActivity.this.finish()
        );

        dialog.setCanceledOnTouchOutside(true);

        new Handler().postDelayed(() -> {
            dialog.dismiss();
            AddDishActivity.this.finish();

        }, 3000);

    }
}
