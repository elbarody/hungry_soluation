package com.hungry.oem.hungrysoluation.view.fragment.fragment_driver;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hungry.oem.hungrysoluation.Model.DishesData;
import com.hungry.oem.hungrysoluation.Model.SalesData;
import com.hungry.oem.hungrysoluation.R;
import com.hungry.oem.hungrysoluation.adapters.DishesToOrderRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.adapters.adapter_drivers.SalesDriverRecyclerViewAdapter;
import com.hungry.oem.hungrysoluation.view.Activity.ItemInCartActivity;
import com.hungry.oem.hungrysoluation.view.fragment.HomeFragment;
import com.hungry.oem.hungrysoluation.webService.RetrofitWebService;
import com.hungry.oem.hungrysoluation.webService.model.response.DishesResponse;
import com.hungry.oem.hungrysoluation.webService.model.response.driver.SalesResponse;
import com.marcinorlowski.fonty.Fonty;
import com.tuyenmonkey.mkloader.MKLoader;
import com.xwady.core.helpers.D;
import com.xwady.core.helpers.SessionDriverManager;
import com.xwady.core.helpers.SessionManager;
import com.xwady.core.helpers.Utils;
import com.xwady.core.models.Driver;
import com.xwady.core.models.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomefragmentDriver.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomefragmentDriver#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomefragmentDriver extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    List<SalesData> salesData = new ArrayList<>();
    RecyclerView rec_sales ;
    SalesDriverRecyclerViewAdapter salesDriverRecyclerViewAdapter ;
    Date date ;
    String current , weekly , monthly;
    Driver driver;
    Unbinder unbinder;
    @BindView(R.id.day)
    Button tv_day ;
    @BindView(R.id.month)
    Button tv_month;
    @BindView(R.id.week)
    Button tv_week ;


    private OnFragmentInteractionListener mListener;
    private User user;
    SessionManager sessionManager;

    public HomefragmentDriver() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomefragmentDriver.
     */
    // TODO: Rename and change types and number of parameters
    public static HomefragmentDriver newInstance(String param1, String param2) {
        HomefragmentDriver fragment = new HomefragmentDriver();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_homefragment_driver, container, false);
        unbinder = ButterKnife.bind(this, view);

        sessionManager=new SessionManager(getContext());
        user=sessionManager.getUserDetails();
        SessionDriverManager sessionDriverManager = new SessionDriverManager(HomefragmentDriver.this.getContext());
        driver = sessionDriverManager.getDriverDetails();

        rec_sales = view.findViewById(R.id.rec_sales);
        date = new Date();

        Calendar week = Calendar.getInstance();
        week.setTime(date);
        week.add(Calendar.DATE, -7);
        Date dateBefore7Days = week.getTime();

        Calendar month = Calendar.getInstance();
        month.setTime(date);
        month.add(Calendar.DATE, -30);
        Date dateBefore30Days = month.getTime();

        weekly = new SimpleDateFormat("yyyy-MM-dd").format(dateBefore7Days);
        monthly = new SimpleDateFormat("yyyy-MM-dd").format(dateBefore30Days);
        current= new SimpleDateFormat("yyyy-MM-dd").format(date);


        getSalesRetrofit(current,current,driver.id );

        salesDriverRecyclerViewAdapter = new SalesDriverRecyclerViewAdapter(salesData ,HomefragmentDriver.this.getContext(), "");
        rec_sales.setLayoutManager(new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL, false));
        rec_sales.setAdapter(salesDriverRecyclerViewAdapter);

        rec_sales.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                rv.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });

        Fonty.setFonts((ViewGroup) view);

        return view;
    }

    @SuppressLint("ResourceAsColor")
    @OnClick({R.id.day,R.id.month , R.id.week})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.day:
                tv_day.setBackgroundResource(R.drawable.border_right);
                tv_month.setBackgroundResource(R.drawable.border);
                tv_week.setBackgroundResource(R.drawable.border);
                getSalesRetrofit(current,current,driver.id );
                break;
            case R.id.week:
                tv_day.setBackgroundResource(R.drawable.border);
                tv_month.setBackgroundResource(R.drawable.border);
                tv_week.setBackgroundResource(R.drawable.border_right);
                getSalesRetrofit(current,weekly,driver.id );
                break;
            case R.id.month:
                tv_day.setBackgroundResource(R.drawable.border);
                tv_month.setBackgroundResource(R.drawable.border_right);
                tv_week.setBackgroundResource(R.drawable.border);
                getSalesRetrofit(current,monthly,driver.id);
                break;
        }


    }


    private void getSalesRetrofit(String from , String to , String id) {
        //loader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(D.ORDER_URL).getAllSales(from ,to , id,user.lang).enqueue(new Callback<SalesResponse>() {
            @Override
            public void onResponse(Call<SalesResponse> call, Response<SalesResponse> response) {
                if (null == response.body()) {
                    onFailure(call, new Throwable());
                    //loader.setVisibility(View.GONE);
                    return;
                } else if(response.body().errorCode.equals("3")){
                    Toast.makeText(getContext(),response.body().message,Toast.LENGTH_LONG).show();
                    //loader.setVisibility(View.GONE);
                }else if (!response.body().data.isEmpty()) {
                    salesData.addAll(response.body().data);
                    salesDriverRecyclerViewAdapter.notifyDataSetChanged();
                    //loader.setVisibility(View.GONE);
                }
                //loader.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<SalesResponse> call, Throwable t) {
                Utils.showSnackbar(getActivity(), R.string.no_internet_connection);
                //loader.setVisibility(View.GONE);
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
